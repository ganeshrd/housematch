#!/bin/bash
# This files inserts all the media queries mixins at the end of each scss component
# and list the @include lines to copy paste in _hm-media-queries.scss
# @author Louis-Ro <louisrom1@gmail.com>
# execute with : $ ./script.sh

NAMES=()

echo "---------------------------NAMES:--------------------------------------" >&2

for i in **/*.scss *.scss; do
	# for i in *.scss; do
# for i in _hm-globals.scss; do

	if [ "$i" == "_hm-media-queries.scss" ] ; then
          continue;
    fi
    name=${i##*/} # remove dir name
	name=${name%.scss} # remove extension
	name=`echo "$name" | sed -n "s/^_hm-//p"` # remove _hm-
	if [[ $name = *"."* ]]; then
		name=`echo "$name" | sed -n 's/\./-/p'`  #replace . to --
	fi

	echo "$name" >&2

	NAMES+=($name)

	text="
// mixins called in _hm-media-queries.scss
@mixin media-"$name"--phones-prtrt() {
}
@mixin media-"$name"--phones-lndscp() {
}"  
	echo "$text" >> $i
done

echo "---------------------------@includes portrait:--------------------------------------" >&2

for j in "${NAMES[@]}"
do
	echo "@include media-"$j"--phones-prtrt();"
done

echo "---------------------------@includes landscapes:--------------------------------------" >&2


for j in "${NAMES[@]}"
do
	echo "@include media-"$j"--phones-lndscp();"
done