/** 
 * Interface N2O / hm_app.
 * Définition de la fonction de traitement du protocole de formalisation N2O '$bin'.
 * @author Renaud Tripathi
 */

$bin.do = function ws_recv(recv) {
  let resp = JSON.parse(recv.v[1].v);
  hm_bridge.data_transfer(resp.id_t,resp);
};
