/**
 * debug utils.
 * Include this file after n2o's scripts include.
 * @author Renaud Tripathi
 */

// stringify
var dumpObject = function(o) { return JSON.stringify(o); };

// dump an array of objects
var dumpObjects = function(a) {
  d = a.map(function(x) {return JSON.stringify(x);});
  return str = "{ " + d.join(',') + " }";
};

// return name's objects
var objectNames = function(a) {
  d = a.map(function(x) {return x.objectName;});
  return str = "{ " + d.join(',') + " }";
};

// bert encoder wrapper
// encode une liste à partir d'un array
var arrayToList = function(a) { if(a) return list.apply(null,a);
                                else return list(); };

/**
 * Encodage bert des objets sous la forme: {main_k,{k,v},{k,v}}
 * @param name La clé princincipale
 * @param o L'objet à encoder
 */
var hm_tupleize_object = function(name,o) {
  l_entries = o.keys().map(function(k) { return tuple(bin(k),bin(o.k)); });
  return tuple(atom(name),arrayToList(l_entries));
};

/**
 * Encodage bert d'un objet en proplist erlang.
 * @param obj Un objet de la forme { k:v }.
 * Chaque couple k,v est encodé : tuple(atom(k),bin(v)) à
 * l'exception des clés : [action] => tuple(atom(k),atom(v)),
 *                        [timestamp,period,charge,goods,surface,from] => tuple(atom(k),number(v))
 *                        [geo] => tuple(atom('geo'),list( tuple(atom('type'),bin(geo.type)),
 *                                                         tuple(atom('coordinates'),list(number(geo.coordinates[0],geo.coordinates[1]))) ) )
 * @see bert.js.
 * @return Encodage du terme erlang [ {k,v} ].
 */
var object_to_proplist = function(o) {
  let a = _.entries(o), atom_keys = [ 'action','for','intent','furnitures','period_type','status','timeframe'], 
                        num_keys = [ 'timestamp','charge','surface','from','renting','duration','goods_type','age','floor','rooms','beds','goods'],
                        object_keys = [ 'description','period','add_state','data' ], bin_list = [ 'images', 'blacklist' ];
  prop = a.map(function(t) { if(atom_keys.indexOf(t[0]) >=0) return tuple( atom(t[0]),atom(t[1].toString()) ); // bench if vs toString for every one
                             else if(num_keys.indexOf(t[0]) >=0) return tuple( atom(t[0]),number(parseInt(t[1],10)) ); // ensure number
                             else if(object_keys.indexOf(t[0]) >=0) return tuple( atom(t[0]),object_to_proplist(t[1]) );
                             else if(bin_list.indexOf(t[0]) >=0) return tuple( atom(t[0]), arrayToList(t[1].map(function(s) { return bin(s); })) );
                             else if(t[0] == 'geo') return tuple( atom('geo'),list( tuple(atom('type'),bin(t[1].type)),
                                                                                    tuple(atom('address'),bin(t[1].address)),
                                                                                    tuple(atom('full_address'),atom(t[1].full_address.toString())),
                                                                                    tuple( atom('coordinates'), arrayToList(t[1].coordinates
                                                                                      .map(function(c) { return bin(c); })) )));
                             else return tuple( atom(t[0]),bin(t[1]) ); });
  return arrayToList(prop);
}

var list_to_binary = function(l) {
  return String.fromCharCode.apply(null,l);
};

/**
 * Decodage du champ meta encodé côté client.
 * @see bert.js
 */
var meta_decode = function(m) {
  let proto = m.v.map(function(l) { let o = {}, k = list_to_binary(l.v[0].v);
            o[k] = list_to_binary(l.v[1].v);
            return o;} );
  let meta = Object.assign.apply(null,proto);
  return meta;
};

/**
 * Decodage des meta encodées côté serveur.
 * @see bert.js
 */
var meta_to_object = function(proto) {
  let acc = {};
  proto.forEach(function(o) { if(o.t == 104) acc[o.v[0].v] = o.v[1].v;}); return acc; };

/**
 * Decodage des meta-informations des fichiers téléchargés depuis le serveur.
 */
var file_info_decode = function(proto) {
  return proto.map(function(o) { return meta_to_object(o.v); });
};

/**
 * Mise en forme JSON des message serveur.
 */
var to_json = (s) => { return JSON.parse(s.replace(/\\"/,"'")); }

/**
 * Decodage et mise en forme d'un tableau d'entité.
 * @param Array a Une collection d'obet de la forme {t:107,v:jsone}.
 */
var to_json_array = (a) => { return a.map((e) => { return to_json(e.v); });  };

var hm = { dbg_object : dumpObject, dbg_objects : dumpObjects,
           dbg_names : objectNames, array_to_list : arrayToList,
     list_to_binary : list_to_binary, meta_decode : meta_decode,
     to_json: to_json, to_json_array: to_json_array,
     meta_to_object : meta_to_object, object_to_proplist:object_to_proplist,
     file_info_decode:file_info_decode};
