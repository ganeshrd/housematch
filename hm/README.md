# Librairie du projet HM\_MOBILE

Cette librairie abrite:

  * Les définitions des parties spécifique de la librairie client N2O ( `n2o_bin_do.js` ).
  * La redéfinition des éléments de la librairie client N2O
  * Les fonctions d'interfaçage N2O / ionic.
  * Des fonctions utilitaires ( `hm_utils.js` )

### Définitions des parties spécifiques

Il s'agit des fonctions de traitement des protocoles de formalisation.

### Redéfinition des éléments de la librairie client N2O

`hm_n2o.js` et `hm_ftp.js` sont issue de modifications des scripts `n2o.js` et `ftp.js` respectivement.

**hm\_ftp.js n'est plus maintenu**

### Fonctions d'interfaçage N2O / ionic

Il s'agit des fichiers `hm_auth.js`, `hm_client.js`, `hm_dev.js`, `hm_register.js`.
Chaque fichier correspond à un module serveur éponyme. Les fonctions de traitement de ces fichier
répercutent les chagements d'état dans l'éco-système ionic via la variable globale `hm\_bridge`.

### Fonction utilitaires

Il s'agit du script `hm_utils.js` qui propose des fonctions de mise en forme des données en provenance
et en partance du serveur.
Dans le cadre du projet HM\_MOBILE, la fonction principalement utilisée est `object_to_proplist`.
