angular.module("hm_components")

/**
 * Ce composant gère l'affichage des annonces de l'utilisateur dans une liste.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.component('userAddItem',
  {
    controller : [ '$log','$translate',function ($log,$translate) {
                   let self = this;
		               self.rentings = { 2: $translate.instant('rental'), 3:$translate.instant('sublease'),5:$translate.instant('shared') };
		               self.address = () => { return self.add.geo.address; };
		               self.add_type = () => { return self.add.for == 'tenant'? $translate.instant('proposal') : $translate.instant('search'); };
		               self.renting_type = () => { let renting = [2,3,5].map(v => { if(self.add.renting % v == 0) return self.rentings[v]; });
		                                           return renting.join(' '); };
                 }],
    templateUrl: 'templates/components/user-add-item.html',
    bindings : {
                 add: '='
               }
  });
