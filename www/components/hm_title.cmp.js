angular.module("hm_components")

/**
 * Ce composant affiche le titre.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.component('hmTitle',
  {
    templateUrl : 'templates/components/title.html',
    bindings : {
                 title : '=',
                 subTitle : '='
               }
  });
