/**
 * Module de déclaration des composants Housematch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
angular.module("hm_components", ['hm_filters']);
// sic ...
