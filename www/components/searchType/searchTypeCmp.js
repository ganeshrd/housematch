angular.module("HouseMatchComponents", []).component("searchType", {
  bindings: {
    types: "=",
    readOnly: "<"
  },
  controller: function helloWorldCtrl() {

    let self = this;

    self.selectType = (value) => {
      if(self.readOnly){
        return;
      }
      if(self.isAllSelected()){
        self.selectAllType();
      }
      self.types[value] = !self.types[value];
    }

    self.selectAllType = () => {
      if(self.readOnly){
        return;
      }
      if(self.isAllSelected()) {
        self.types = {
          2 : false,
          3 : false,
          5 : false
        };
        return;
      }
      self.types = {
        2 : true,
        3 : true,
        5 : true
      };
      console.log("test-self.types",self.types);
    }

    self.isAllSelected = () => {
      return self.types[2] && self.types[3] && self.types[5];
    }
  },
  templateUrl: "templates/components/searchType.html",
  controllerAs: "ctrl"
});
