angular.module("hm_components")

/**
 * Encart publicitaire à destination des annonceurs
 * Ce composant est utilisé dans le fil d'actualité
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.component('hmAddvert',
  { 
    templateUrl : 'templates/components/hm-addvert.html',
    bindings : {
                 addvert : '='
               }
  });
