angular.module("hm_components")

/**
 * Ce composant gère l'affichage des sectionns du menu.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.component('menuItem',
  { 
    templateUrl : 'templates/components/menu-item.html',
    bindings : {
                 item : '=',
               }
  });
