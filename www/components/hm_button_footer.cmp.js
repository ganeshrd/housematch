angular.module("hm_components")

/**
 * Ce composant gère l'affichage des annonces dans une liste.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.component('hmButtonFooter',
  {
    controller : function HmButtonFooterCtrl() {
                 let self = this;

                 self.disabled = false;

                 },
    templateUrl : 'templates/components/hm_button_footer.html',
    bindings : {
                 title: '<',
                 disabled: '<',
                 subTitle: '<'
               }
  });
