angular.module("hm_components")

/**
 * Ce composant gère l'affichage des annonces dans une liste.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.component('addItem',
  { 
    templateUrl : 'templates/components/add-item.html',
    bindings : {
                 data : '='
               }
  });
