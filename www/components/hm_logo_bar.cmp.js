angular.module("hm_components")
/**
 * Ce composant gère l'affichage de la bar logo.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.component('hmLogoBar',
  {
    controller : function hmBarLogoCtrl() {
                 let self = this;
                 },
    templateUrl : 'templates/components/hm_logo_bar.html',
    controllerAs: "ctrl",
    bindings : {
                 title: '=',
                 leftCb: "&",
                 leftName: "=",
                 rightCb: "&",
                 rightName: "=",
                 rightIcon: "="
               }
  });
