angular.module('hm_filters')
/**
 * Filtre de traduction automatique de l'application HouseMatch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.filter('hm_translation', function() {
  /** 
   * @todo
   */
  return function (input) { return input; };
});
