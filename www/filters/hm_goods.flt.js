angular.module('hm_filters')
/**
 * Conversion des valeurs goods, furnitures du formulaire
 * de recherche au format humain.
 * Affiche une chaîne par défaut si le filtre est appliqué sur une donnée null.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.filter('hm_goods',['$log','$translate',function($log,$translate) {
  return function(goods) {
        stmt = (goods == 0)? $translate.instant('filter.goods.0') : $translate.instant('filter.goods.2',{room : goods + 1}); 
        return stmt;
  };

}]);

