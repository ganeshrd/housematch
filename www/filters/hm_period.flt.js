angular.module('hm_filters')
/**
 * Conversion des objets 'period' des formulaires de recherche au format humain.
 * Affiche une chaîne par défaut si le filtre est appliqué sur une donnée null.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.filter('hm_period',['$log','$filter','$translate',function($log,$filter,$translate) {

  return function(data) {
    let stmt;
    if(data.timeframe)
    {
      // let f = $filter('date')(data.from,'dd MMM'), t = $filter('date')(data.to,'dd MMM');
      let period = moment(data.to).diff(moment(data.from));
      //stmt = $translate.instant('filter.period.1',{from:f}) + ' ' + $translate.instant('filter.period.2',{to:t});
      stmt = moment.duration(period).humanize();
    }
    else
    {
      stmt = $translate.instant('filter.period.0');
    }
    return stmt;
  }; 
}]);
