angular.module('hm_filters')
/**
 * Conversion des valeurs goods, furnitures du formulaire
 * de recherche au format humain.
 * Affiche une chaîne par défaut si le filtre est appliqué sur une donnée null.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.filter('renting',['$log','$translate',function($log,$translate) {
  let self = this;

  return function(renting) {
    let o = { 2 : 'rental',
              3 : 'sublease',
              5 : 'shared'
            };
    let keys = Object.keys(o).reduce((acc,v) => { if(renting % v == 0) acc.push($translate.instant(o[v]));
                                                  return acc; },[]);
    return keys;
  };
}]);

