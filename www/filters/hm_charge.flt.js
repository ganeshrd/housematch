angular.module('hm_filters')
/**
 * Affichage des montant de loyer.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.filter('hm_charge',['$log','$filter','$translate',function($log,$filter,$translate) {

  return function(data) {
    let stmt;
      stmt = $translate.instant('filter.charge.0') + ' : ' + $filter('currency')(data,"€",0);
    return stmt;
  }; 
}]);
