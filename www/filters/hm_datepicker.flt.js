angular.module('hm_directives')
/**
 * Converiton des timestamps UNIX en format humainement lisible. 
 * Affiche une chaîne par défaut si le filtre est appliqué sur
 * une donnée non définie.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.filter('hm_datepicker',['$log','$filter','$translate',function($log,$filter,$translate) {
  return function(d) {
    if((angular.isNumber(d) && !isNaN(d)) || angular.isString(d)) return $filter('date')(d,'shortDate');
    else return $translate.instant('filter.from.0');
  };
}]);
