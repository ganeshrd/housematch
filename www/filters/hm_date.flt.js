angular.module('hm_filters')
/**
 * Filtre de traduction automatique de l'application HouseMatch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.filter('hm_date', [ '$log','$filter',function($log,$filter) {
  
  /**
   * converti un timestamp au format humain et ajoute 
   * un incipit et une coda optionnellement
   * @param unix Integer un timestamp unix
   * @param pre String Une chaîne de caractère ajoutée en début.
   */
  return function(unix) { return $filter('date')(unix,'fullDate'); };
}]);
