angular.module('hm_filters')
/**
 * Conversion des objets 'geo' des formulaires de recherche au format humain.
 * Affiche une chaîne par défaut si le filtre est appliqué sur une donnée null ou indéfinie.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.filter('hm_geo',['$log','$translate',function($log,$translate) {

  return function(data) {
    if(angular.isUndefined(data) || angular.isUndefined(data.address)) return $translate.instant('filter.geo.0');
    let address = angular.isDefined(data.displayed_address)? data.displayed_address : data.address; 
    return address;
  }; 
}]);
