// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', [
                           'ionic','ionic.native', // ,'ionic-datepicker',
			                     'pickadate', 'angularMoment','ngStorage',
                           'pascalprecht.translate', 'ionic-multi-date-picker',

			                     'hm_constants',
                           'hm_filters',
			                     'hm_directives',
                           'hm_components',

                			     'nav',
			                     'menu',
                           'register-number',
                           'share',

			                     'init',
                           'exit',
			                     'register',

			                     'profile',
                  			   'news',
                  			   'messages',
                  			   'match',

                  			   'adds',
                  			   'form',
                  			   'faq',
                  			   'contact',
                  			   'parameters',

                  			   'detail',

                  			   'tests',
                           'arnaudspanneut.ionicPushNotifications',
                           'ngImgCrop',
                           'ion-google-autocomplete'
])

.controller('MainCtrl',['$log','$scope','$state',MainCtrl])

.factory('WireSvc',['$q','FTP_TIMEOUT','WS_TIMEOUT','MAX_ATTEMPT','$timeout','$log',WireSvc])
.factory('DbSvc',['DATABASE','DB_SIZE','$cordovaSQLite','$q','$ionicPlatform','$log',DbSvc])
.factory('KvsSvc',['$log','$q','$localStorage',KvsSvc])
.factory('ModalsSvc',['$log','$ionicPopup','$ionicModal','$translate', '$q', ModalsSvc])
.factory('SessionSvc',['$log','$cacheFactory','$state','DataSvc',SessionSvc])
.factory('RegisterSvc',['$log','$q','CryptoSvc','DataSvc','KvsSvc',RegisterSvc])
.factory('FirebaseSvc',['$log','$q','FIREBASE_PARAMS','NotificationSvc', FirebaseSvc])
.factory('ImgSvc',['$cordovaCamera','$log','$q','$cordovaActionSheet','$translate',ImgSvc])
.factory('LocalesSvc',LocalesSvc)
.factory('CryptoSvc',['$log','md5','KvsSvc',CryptoSvc])
.factory('UsersSvc',['$log','DataSvc', '$q', 'SessionSvc', 'FirebaseSvc', 'KvsSvc',UsersSvc])
.factory('DataSvc',['$log','$state','$q','$ionicLoading','CryptoSvc','WireSvc','KvsSvc','ModalsSvc',DataSvc])
.factory('AuthSvc',['$q','$log','$state','DataSvc','KvsSvc','SessionSvc',AuthSvc])
.factory('PermissionSvc',['$q', '$ionicPlatform', PermissionSvc])
.factory('FormsSvc',['$log','$timeout','DataSvc',FormsSvc])
.factory('NotificationSvc',['PermissionSvc', NotificationSvc])
.factory('LocalisationSvc',['$q', LocalisationSvc])
.factory('RedirectionSvc',['DataSvc', 'KvsSvc', RedirectionSvc])
.factory('MessageSvc',['FirebaseSvc', 'FIREBASE_PARAMS', 'SessionSvc', '$q', 'DataSvc', 'UsersSvc', MessageSvc])
.factory('DatepickerSvc',['$log','$translate',DatepickerSvc])

  .config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
  })

  .run(function($log,$q,$rootScope,$state,$ionicPlatform,$ionicHistory,$cordovaKeyboard,$translate,amMoment,
                STATE_BACK_DISABLE,LOCALE_REGEX,SUPPORTED_LANGUAGES,AuthSvc,KvsSvc,WireSvc,FirebaseSvc, RedirectionSvc, MessageSvc, NotificationSvc) {

    FirebaseSvc.initialize();
    $ionicPlatform.ready(function() {

    if(window.StatusBar) { StatusBar.styleDefault(); }
    if(window.cordova)
    {
      setTimeout(function() {
           navigator.splashscreen.hide();
       }, 2000);
      if($cordovaKeyboard)
      {
        $cordovaKeyboard.hideKeyboardAccessoryBar(true);
        // Stops the viewport from snapping when text inputs are focused.
        // Ionic handles this internally.
        $cordovaKeyboard.disableScroll(true);
      }
      $ionicPlatform.registerBackButtonAction(function (event) {
        if(STATE_BACK_DISABLE.indexOf($state.current.name) === -1)
        {
          event.preventDefault();
          $ionicHistory.goBack();
        }
      }, 101);
    }

    // routing logic
    redirectUser = () => {
           RedirectionSvc.checkRedirection().then(val => {
             // $state.go('nav.message', {tel: "+33711111111"});
             // $state.go('register.tel');
             $state.go(val);
           });
     };
    // determine user language
    am_setup = () => {
      let match = navigator.language.match(LOCALE_REGEX);
      let user_lang = match? match[1] : 'en';
      let lang_setup = SUPPORTED_LANGUAGES.indexOf(user_lang) >= 0? user_lang : 'en';
      amMoment.changeLocale(lang_setup);
      moment.lang('fr', {
        relativeTime : {
          past:   "%s"
        }
      });
    };

    $rootScope.goBack =  () => { window.history.back(); }; // nav back available everywhere

    am_setup();
    WireSvc.link(); // just once
    var hm_bridge = {}; // N20 / angularjs interface

    $log.info("N2O start");
    N2O_start().then( () => { redirectUser(); },
                     err => {
                              $log.error('app.js ' + err);
                              let opt = { title : $translate.instant('modal.network_failure.0'),
                                          template : $translate.instant('modal.network_failure.1'),
                                          okText : $translate.instant('btn_retry'),
                                          cancelText : $translate.instant('btn_abort') };
                                          // TODO
                            });

  });
})

.config(function($urlRouterProvider,$translateProvider,LOCALE_REGEX,SUPPORTED_LANGUAGES,english_version,french_version) {
  $urlRouterProvider.otherwise('/init');
  $translateProvider
    .translations('en', english_version)
    .translations('fr', french_version)
    .determinePreferredLanguage(() => {
                                        let match = navigator.language.match(LOCALE_REGEX);
                                        let user_lang = match? match[1] : 'en';
                                        return SUPPORTED_LANGUAGES.indexOf(user_lang) >= 0? user_lang : 'en';
                                      });
});
