angular.module('share',['angular-md5','hm_filters'])

.config(function($stateProvider) {
  $stateProvider

    .state('share',
           { url: '/share',
             templateUrl: 'templates/share/share.html',
             controller: 'ShareCtrl as ctrl'
            })

})
.controller('ShareCtrl',['$log','$scope','$state','DataSvc', '$stateParams', ShareCtrl]);
