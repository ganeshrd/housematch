/**
 * page pour inciter les utilisateur à partage l'application .
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var ShareCtrl = function($log,$scope,$state,DataSvc, $stateParams, $cordovaSms, $cordovaSocialSharing) {
  var self = this;
  self.step = 1;
  let message = "Bonjour,\n j’utilise HouseMatch, l’application qui met en relation les personnes de son réseau qui cherchent ou proposent un logement.\n"
                + "Faites suivre ce message et rendez-vous sur http://example.com pour télecharger l’application et trouver un match.";
  self.next = () => {
    console.log('self.step', self.step);
    if(self.step == 3) {
      $state.go('nav.news');
      return;
    }
    self.step++;
  }

  self.sendSms = () => {

        console.log("number=" + number + ", message= " + message);
        let me
        //CONFIGURATION
        var options = {
            replaceLineBreaks: true, // true to replace \n by a new line, false by default
            android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
                //intent: '' // send SMS without open any other app
            }
        };

        var success = function () { alert('Message envoyé avec succès'); $state.go('nav.news'); };
        var error = function (e) { alert('Message Failed:' + e); };
        sms.send('', message, options, success, error);

  }

  self.skip = () => {
    $state.go('nav.news');
  }
  
  self.shareWithFB = () => {
    facebookConnectPlugin.showDialog({
    	method: "share",
    	href: "http://example.com",
    	description: message,
    	picture: 'https://i2.au.reastatic.net/800x450-crop/e47b769f259d179c71c9a238b0baf8a07f5f4f2ae1235c6a12f55b5deb7b0745/main.jpg',
    	hashtag: '#location'
    },
    () => {alert('Message envoyé avec succès'); $state.go('nav.news');},
    () => {console.log('failure');})
  }


};
