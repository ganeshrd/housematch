/**
 * Controleur du formulaire du nouvel utilisateur.
 * Dans un cas optimum, le contrôleur ne sera chargé qu'une seule fois.
 * Le formulaire d'inscription est un formulaire simplifié. Pas de gestion
 * des images, pas de description.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Louis-Ro <louisrom1@gmail.com>
 */
 var AddCtrl = function($log,$scope,$state,DataSvc,FormsSvc,SessionSvc) {

  let self = this;

  $scope.validation = { btn_disabled : false };
  $scope.add = FormsSvc.new_form('tenant'); // form will for lessors by default
  self.step = 1;

  var weekDaysList = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
  var monthList = ["janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];

  var h0 = new Date(2018, 11, 11)
  , h1 = new Date(2018, 11, 9)
  , h2 = new Date(2018, 11, 3)
  , h3 = new Date(2018, 11, 10)
  , h4 = new Date(2018, 10, 30)
  , h5 = new Date(2018, 11, 16)
  , h6 = new Date(2018, 11, 6)
  , calendar0 = [];

  var c0 = new Date(2018, 11, 11)
  , c1 = new Date(2018, 11, 9)
  , c2 = new Date(2018, 12, 3)
  , c3 = new Date(2018, 11, 10)
  , c4 = new Date(2018, 11, 12)
  , c5 = new Date(2018, 11, 16)
  , c6 = new Date(2018, 11, 18)
  , c7 = new Date(2018, 11, 19)
  , c8 = new Date(2018, 11, 22)
  , c9 = new Date(2018, 11, 27)
  , c10 = new Date(2018, 11, 25)
  , c11 = new Date(2018, 11, 6)
  , calendar1 = [c0, c1]
  , calendar2 = [c2, c3]
  , calendar3 = [c4]
  , calendar4 = [c2, c5, c11]
  , calendar5 = [c4, c10]
  , calendar6 = [c6, c7, c8, c9]
  , calendar7 = [c5, c6, c11];
  calendar1 = [];
  calendar2 = [];
  calendar3 = [];
  calendar4 = [];
  calendar5 = [];
  calendar6 = [];
  calendar7 = [];

  var d0 = new Date(2018, 11, 16)
  , d1 = new Date(2018, 11, 17)
  , d2 = new Date(2018, 11, 17)
  , d3 = new Date(2018, 10, 30)
  , d4 = new Date(2015, 12, 1)
  , disabledDates = []; //no disabled dates

  var s0 = new Date(2018, 10, 31)  // preview month
  , s1 = new Date(2018, 11, 10) // holiday
  , s2 = new Date(2018, 11, 11) // holiday
  , s7 = new Date(2018, 11, 6) //
  , s3 = new Date(2018, 11, 12) //
  , s4 = new Date(2018, 11, 12) // clone
  , s5 = new Date(2018, 11, 17) // conflict with disabled
  , s6 = new Date(2018, 12, 1); // conflict with disabled, next month
  // $scope.selectedDates = [s1, s2, s3, s4, s0, s5, s6, s7];
  $scope.selectedDates = [];  //no selected dates by default


  $scope.datepickerObject = {
    templateType: 'MODAL', // POPUP   | MODAL
    modalFooterClass: 'bar-clear',
    header: 'Dates',
    headerClass: 'balanced-bg dark',

    btnsIsNative: false,

    btnOk: 'Valider',
    btnOkClass: 'button-outline button-balanced',

    btnCancel: 'Annuler',
    btnCancelClass: 'button-clear button-dark',

    btnTodayShow: false,
    btnToday: 'Aujourd\'hui',
    btnTodayClass: 'button-clear button-dark',

    //btnClearShow: true,
    btnClear: 'Clear',
    btnClearClass: 'button-clear button-dark',

    selectType: 'MULTI', // SINGLE | PERIOD | MULTI

    tglIndeterminatePeriodShow: true, // true | false (default)
    tglIndeterminatePeriod: 'Période indéterminée',
    isIndeterminatePeriod: true, // true (default) | false
    tglIndeterminatePeriodClass: 'toggle-balanced',
    titleIndeterminatePeriodClass: 'dark',

    tglSelectByWeekShow: false, // true | false (default)
    tglSelectByWeek: 'Semaines entières',
    isSelectByWeek: false, // true (default) | false
    selectByWeekMode: 'NORMAL', // INVERSION (default), NORMAL
    tglSelectByWeekClass: 'toggle-positive',
    titleSelectByWeekClass: 'positive positive-border',

    accessType: 'WRITE', // READ | WRITE
    //showErrors: true, // true (default), false
    //errorLanguage: 'RU', // EN | RU

    //fromDate: new Date(2015, 9),
    //toDate: new Date(2016, 1),

    selectedDates: $scope.selectedDates,
    //viewMonth: $scope.selectedDates, //
    disabledDates: disabledDates,

    calendar0: calendar0,
    calendar0Class: '',
    calendar0Name: 'holidays',

    calendar1: calendar1,
    //calendar1Class: '',
    calendar1Name: 'same days',

    calendar2: calendar2,
    calendar2Class: '',
    //calendar2Name: 'calendar 2',

    conflictSelectedDisabled: 'DISABLED', // SELECTED | DISABLED

    closeOnSelect: false,

    mondayFirst: true,
    weekDaysList: weekDaysList,
    monthList: monthList,

    callback: function (dates, period) {  //Mandatory
      retSelectedDates(dates, period);
    }
  };

  var retSelectedDates = function (dates, period) {
    // console.log("period", period)
    $scope.selectedDates.length = 0;
    for (var i = 0; i < dates.length; i++) {
      $scope.selectedDates.push(angular.copy(dates[i]));
    }
    $scope.periodIndeterminee = period;
  };



  self.next = () => {
    if(self.isDisabled()) {
      return;
    }
    if(self.step == 2) {
      self.publish();
      return;
    }
    self.step++;
  };

  self.isDisabled = () => {
    if(self.step == 1) {
      return !$scope.add || $scope.add.renting == 1 || !$scope.add.intent;
    } else if(self.step == 2) {
      return !$scope.add || !$scope.add.geo || !$scope.add.goods || !$scope.selectedDates || $scope.selectedDates.length == 0 || !$scope.add.charge;
    }
    return false;
  };
  /**
   * Envoi de l'annonce de l'utilisateur
  */
  self.publish = () => {
    $scope.validation.btn_disabled = true;
    let booktime = { from : Math.round($scope.selectedDates[0].constructor.now()/1000),
                     timeframe : (!$scope.periodIndeterminee).toString(),
                   };
    if($scope.selectedDates.length == 2)
    {
      booktime.to = Math.round($scope.selectedDates[1].constructor.now()/1000);
    }
    $scope.add.period = booktime;
    FormsSvc.publish($scope.add).then(publish_result).catch(err => { $state.go('system_failure') });
  };

  function publish_result(resp) {
      // $log.info('add.ctrl.js server status is ' + resp.status);
      self.state_update();
  };

  /**
   * Mise à jour de la variable d'état de l'utilisateur
   */
  self.state_update = () => { let mandatories = { action : 'user_state' };
                              DataSvc.send(mandatories).then(resp => { SessionSvc.set_user_state(resp.value);
                                                                       $state.go('nav.news'); })
                                                       .catch(err => { $state.go('system_failure'); }); };
};
