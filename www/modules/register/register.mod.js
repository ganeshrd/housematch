angular.module('register',['angular-md5','hm_filters'])

.config(function($stateProvider) {
  $stateProvider
    .state('register',
           { url: '/register',
             templateUrl: 'templates/register/register.html',
	           abstract : true })

    .state('register.comment',
           { url: '/register/comment',
             templateUrl: 'templates/register/comment.html',
             controller: 'CommentCtrl as ctrl' })

    .state('register.tel',
           { url: '/register/tel',
	           templateUrl: 'templates/register/tel.html',
	           controller: 'TelCtrl as ctrl' })

    .state('register.challenge',
           { url: '/register/challenge/:tel_status',
	           templateUrl: 'templates/register/challenge.html',
	           controller: 'ChallengeCtrl as ctrl' })

    .state('register.name',
      { url: '/register/name',
        templateUrl: 'templates/register/name.html',
        controller: 'NameCtrl as ctrl' })

    .state('register.add',
      { url: '/register/add',
        templateUrl: 'templates/register/add.html',
        controller: 'AddCtrl as ctrl' });

})
.controller('CommentCtrl',['$log','$state', 'PermissionSvc', CommentCtrl])
.controller('NameCtrl',['$log','$scope','$state','DataSvc', 'UsersSvc', NameCtrl])
.controller('TelCtrl',['$log','$scope','$state','$cordovaContacts','$window',
                       'country_codes','ModalsSvc','RegisterSvc','KvsSvc', 
                       'PermissionSvc', 'NotificationSvc', 'cell_phones_re', TelCtrl])
.controller('ChallengeCtrl',['$log','$scope','$state','$stateParams','RegisterSvc',ChallengeCtrl])
.controller('AddCtrl',['$log','$scope','$state', 'DataSvc','FormsSvc','SessionSvc',AddCtrl]);
