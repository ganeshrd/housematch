/**
 * Page quasi statique avant la registration
 *
 * @author Louis-Ro <louisrom1@gmail.com>
 */
var CommentCtrl = function($log, $state) {

  var self = this;

  self.cancel = () => { $state.go("tutorial"); }
  self.compris = () => { $state.go("register.tel"); }
};
