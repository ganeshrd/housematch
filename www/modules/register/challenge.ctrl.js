/**
 * Identification du terminal de l'utilisateur et inscription.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var ChallengeCtrl = function($log, $scope, $state, $stateParams, RegisterSvc) {

  var self = this;
  self.tel_status = $stateParams.tel_status;
  $scope.binds = { 'code' : null, 'code_status' : 'none' };
  $log.debug('challenge.ctrl.js self.tel_status',self.tel_status);
  $log.debug('challenge.ctrl.js $stateParams',$stateParams);

  self.authenticate = function() { RegisterSvc.id_challenge({action: 'id_challenge'},{'challenge' : $scope.binds.code})
                                         .then(resp => { $log.debug('challenge.ctrl.js resp',resp);switch(resp.status) {
                                                           case 'fatal' : $state.go('system_failure'); // TODO
                                                                          break;
                                                           case 'error' : $scope.binds.code_status = 'error';
                                                                          $scope.binds.code = '';
                                                                          break;
                                                           case 'registered' : RegisterSvc.store_credentials();
                                                                               let state = self.tel_status == 'clear'? 'register.name' : 'nav.news';
                                                                               $state.go(state);
                                                                               break;
                                                           default : $state.go('system_failure');
                                                         } })
                                     };
  self.new_sms = function() { RegisterSvc.new_sms().catch( function() { $state.go('system_failure'); }); };
};
