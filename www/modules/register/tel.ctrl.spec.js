 /**
 * tel.ctrl.js unit tests
 * @author Renaud Tripathi ganeshrd@laposte.net
 */

describe('User registration controller suite',() => {
  
  var $controller, controller;

  beforeEach((_$controller_) => { 
    module('register'); 
    $controller = _$controller_;
    ctrl_init(); });

  it("simple mock module existence test",() => {
    expect(true).toBeTruthy();
    expect(controller).toBeDefined();
  });

  function ctrl_init() { controller = $controller('TelCtrl',{
    $cordovaContacts: null,
    $ionicLoading: null,
    $ionicModal: null,
    $log: null,
    $location: null,
    $scope: null,
    $state: null,
    $stateParams: null,
    $window: null,
    AuthSvc: null,
    cell_phones_re: null,
    countryCodes: null,
    CryptoSvc: null,
    LocalesSvc: null,
    ModalsSvc: null,
    PermissionSvc: null,
    RegisterSvc: null }); };
});
