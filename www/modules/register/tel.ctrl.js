/**
 * Validation du terminal de l'utilisateur et inscription.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var TelCtrl = function($log, $scope, $state, $cordovaContacts, $window, 
                       country_codes, ModalsSvc, RegisterSvc, KvsSvc, 
                       PermissionSvc, NotificationSvc, cell_phones_re) {

  var self = this;

  self.phone_number;
  self.cancel = () => { $state.go("tutorial"); };

  $scope.msg_status = 'no_message';
  $scope.binds = { btn_status : false, dial_code : '+33' };

  $scope.countries = country_codes;
  ModalsSvc.cgu().then(modal => { $scope.cgu = modal; });
  ModalsSvc.privacy().then(modal => { $scope.privacy = modal; });
  $scope.redirectSettings = _redirectSettings; // call from permission warning modal

  function _redirectSettings() { PermissionSvc.redirectSettings(); };

  self.submit_phone_number = (tel) => {
    self.phone_number = $scope.binds.dial_code + tel;
    // let isValid = false;
    // _.forEach(cell_phones_re, val => { let reg = new RegExp(val); if(reg.test(self.phone_number)) isValid = true; });
    let is_valid_number = cell_phones_re.some(function(re) { return re.test(self.phone_number); });
    if(!is_valid_number) return; 
    self.tel_status();
    if ($window.cordova) $window.Keyboard.hide(); 
  };

  self.tel_status = () => { RegisterSvc.id_request(self.phone_number)
                              .then(resp => { $state.go('register.challenge',{tel_status:resp.status}); })
                              .catch(function() { $state.go('system_failure'); }); };

  /**
   * Cette fonction est appelée au chargement du controleur.
   * Elle déclenche la modale de permission d'exportation des contacts.
   * En cas de refus, l'utilisateur est dirigé vers la fermeture de l'application.
   */
  function init() {
    PermissionSvc.requestContactsAuthorization().then( ok => { RegisterSvc.reset_form(); // user_data is empty 
                                                               export_contacts(); },
                                                       err => { console.log('tel.ctrl.js contact permission failure', err); 
                                                                ModalsSvc.displayPermissionWarnnig($scope); });
  };

  /**
   * Affecte l'objet user_data du service RegisterSvc des contacts
   * de l'utilisateur.
   */
  function export_contacts() {
    if ($window.cordova) {
       $cordovaContacts.find(['displayName','phoneNumbers','emails'], { multiple:true })
         .then(cnts => { RegisterSvc.set({ contacts: cnts }); },
               err  => { $log.error('tel.ctrl.js contact failure',err); $state.go('system_failure'); });
    }
    else
    { // mock
      RegisterSvc.set({ contacts: [ { 'displayName':'mock',
                                      'phoneNumbers': [ { type:'tel',value:'+33676142536' },
                                                        { type:'tel',value:'+33729188776' }] } ]});
    }
  };


  init(); // launch on controller loaded
};
