/**
 * Inscription de l'utilisateur.
 * Mise à jour du nom et du prénom de l'utilisateur.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var NameCtrl = function($log,$scope,$state,DataSvc, UsersSvc) {
  var self = this;

  $scope.bindings = { name : '', firstname : '' };
  $scope.validation = { btn_disabled : false };

  function response(resp) {
    $log.info('name.ctrl.js user profile resp is ' + resp.status);
	    $state.go('register.add');
  };

  self.facebookLogin = () => {
    facebookConnectPlugin.login(["public_profile", "email"],(success) => {
      facebookConnectPlugin.api(success.authResponse.userID + "/?fields=first_name,last_name",["public_profile"],
      	 (result) => {
           $scope.$apply(function () {
             $scope.bindings.firstname = result["first_name"];
             $scope.bindings.name = result["last_name"];
           });
        },
    	(error) => {
    		alert("Failed: " + error);
    	});
      $log.info("success", success);
    }, (error) => {
      alert("erro");
    });
  }
  /**
   * Validation du nom et du prénom de l'utilisateur
   */
  self.next = () => {
    if($scope.bindings.firstname.length < 2 || $scope.bindings.name.length < 2)
    {
      self.name_error = true;
      return;
    }
    $scope.validation.btn_disabled = true;
    let mandatories = {'action' : 'set_profile' };
    let args = {'firstname' : $scope.bindings.firstname, 'name' : $scope.bindings.name};
    UsersSvc.saveUserAttr('surname', $scope.bindings.firstname);
    UsersSvc.saveUserAttr('name', $scope.bindings.name);
    DataSvc.send(mandatories,args).then(response).catch(angular.noop());
  };
};
