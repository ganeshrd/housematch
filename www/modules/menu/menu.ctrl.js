/**
 * file: menu.ctrl.js
 * This is the state menu controller
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var MenuCtrl = function($log,$scope) {
  $scope.dev_topics = [ { topic : 'zombie mode', icon : 'params',state : 'test.zombie' },
                        { topic : 'reset db', icon : 'params',state : 'test.db_reset' } ];
  $scope.read_topics = [
    { topic : 'Mon compte', icon : 'profile',state : 'nav.profile' },
    { topic : "Fil d'actualité", icon : 'network', state : 'nav.news' },
    { topic : "Mes matchs", icon : 'match', state : 'nav.match_menu' }
  ];
  $scope.edit_topics = [
    { topic : 'Modifier/Supprimer', sub_topic : 'mes annonces', icon : 'edit-add', state : 'nav.adds' },
    { topic : 'Publier une offre', icon : 'tenant-form', state : "form_create({form_for:'tenant'})" },
    { topic : 'Publier une demande', icon : 'lessor-form', state : "form_create({form_for:'lessor'})" },
    { topic : 'Vos gains', icon : 'savings', state : "nav.savings" },
  ];
  $scope.config_topics = [
    { topic : 'Paramètres', icon : 'params', state : 'nav.params' },
    { topic : 'FAQ', icon : 'faq', state : 'nav.faq' },
    { topic : 'Contactez-nous', icon : 'contact', state : 'nav.contact' },
  ];

};
