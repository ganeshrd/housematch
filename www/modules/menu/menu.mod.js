/**
 * Module responsable de la gestion du menu.
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 */
angular.module('menu',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.menu',
           { url : '/nav/menu',
	     views: { 'navbar-view' : { templateUrl: 'templates/menu/menu.html',
	                                controller:  'MenuCtrl as ctrl' } } });
})

.controller('MenuCtrl', ['$log','$scope',MenuCtrl]);
