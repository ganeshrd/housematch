var MatchListCtrl = function($log,$scope,$state,$stateParams) {
  var self = this;
  
  $scope.add = $stateParams.args.add;
  $scope.matches = $stateParams.args.matches;
  $scope.title = $scope.add.geo.address; // TODO extract zipcode or region from address ?

  self.init = () => { $scope.match_exists = $scope.matches? true : false;
  };
 
 self.init();
};
