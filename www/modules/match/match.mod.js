angular.module('match',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.match_menu',
           { url : '/nav/match_menu',
	     views : { 
	               'navbar-view' : { 
		                         templateUrl: 'templates/match/match_menu.html', 
					 controller: 'MatchMenuCtrl as ctrl' 
				       } 
		     }
           })
    .state('nav.match_list',
           { url : '/nav/match_list',
	     views : {
	               'navbar-view' : {
		                         templateUrl: 'templates/match/match_list.html',
					 controller: 'MatchListCtrl as ctrl'
				       }
		     },
	     params : { args : null }
           });
})

.controller('MatchMenuCtrl',['$log','$scope','$state','SessionSvc','DataSvc',MatchMenuCtrl])
.controller('MatchListCtrl',['$log','$scope','$state','$stateParams',MatchListCtrl]);
