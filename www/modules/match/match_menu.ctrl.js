/**
 * Point d'entrée des résultats de l'utilisateur.
 * Routage vers les matches depuis la liste des annonces de l'utilisateur.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var MatchMenuCtrl = function($log,$scope,$state,SessionSvc,DataSvc) {
  var self = this; 
  $scope.title = "Match";
  $scope.user_adds = SessionSvc.user_adds();
  $scope.user_has_adds = $scope.user_adds? true : false;

  $scope.match_list = (add) => { let _id = add._id;
				 let matches = $scope.matches.filter(a => a.match == _id);
                                 $state.go('nav.match_list',{ 'args' : { 'matches' : matches,
				                                          'add'  : add } }); };

  self.init = () => { $scope.adds = SessionSvc.user_adds();
                      DataSvc.send('hm_client','matches') 
		        .then((rcpt) => { if(rcpt.receipt == 'success')
			                  {
					    $scope.matches = rcpt.data;
					  }
					  else
					  {
					    $scope.matches = [];
					  } },
			      (err) => { $log.error('match_menu.ctrl.js ' + err); }); 
  };

  self.init();
};

