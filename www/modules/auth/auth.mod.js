angular.module('auth',['mdo-angular-cryptography'])

.controller('AuthCtrl',AuthCtrl)
.controller('RegisterCtrl',['$scope','$ionicPopup','$crypto',function($scope,$ionicPopup,$crypto){
  let self = this;
  self.title = "Inscription";
  self.send = function() {
    if(self.pwd != self.confPwd){
      let confirmAlert = $ionicPopup.alert({
        title: "Les mots de passe sont différents",
	templateUrl: "/search.html"});
      confirmAlert.then(function(res){});
    }else{
      let form = { firstName : self.firstName,
                   lastName  : self.lastName,
		   tel       : self.tel,
		   email     : self.email,
		   pwd       : self.crypt(self.email)};
      //console.log(JSON.stringify(form));
      //console.log("ws: %o",ws);
      ws.send(enc(tuple(atom('bin'),tuple(atom('register'),
        tuple(self.tuplise('firstName',self.firstName),
	      self.tuplise('lastName',self.lastName),
	      self.tuplise('tel',self.tel),
	      self.tuplise('email',self.email),
	      self.tuplise('pwd',self.pwd))))));
    }
  };
  self.crypt = function(p){
    return $crypto.encrypt(p,'toto');
  };
  self.tuplise = function(k,v){
    return tuple(atom(k),bin(v));
  };
}])
.service('AuthSvc',AuthSvc)
.config(function($stateProvider){
  $stateProvider
    .state('auth',
           { url: '/auth',
	     templateUrl: 'templates/auth.html',
	     controller: 'AuthCtrl as AuthCtrl' })
    .state('register',
           { url: '/register',
	     templateUrl: 'templates/register.html',
	     controller: 'RegisterCtrl as RegisterCtrl' });
})
.config(['$cryptoProvider',function($cryptoProvider){
  $cryptoProvider.setCryptographyKey('DFLSJD879686');
}]);
