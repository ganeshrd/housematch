var AuthCtrl = function(AuthSvc,$state) {
  this.title = "authentication";
  this.disclamer = AuthSvc.disclamer();
  this.sdk = AuthSvc.sdk();
  this.register = function(next) {
    AuthSvc.register();
    $state.go(next);
  };
};
