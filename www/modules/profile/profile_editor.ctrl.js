/**
 * Controleur d'édition du profil de l'utilisateur.
 *
 * @author Renaud TRipathi <ganeshrd@laposte.net>
 */
var ProfileEditorCtrl = function($log,$scope,SessionSvc,ModalsSvc, $location, $anchorScroll, ImgSvc, FirebaseSvc, DataSvc, $ionicLoading) {
  var self = this;

  self.modal = null; // Instantiée dans ModalsSvc
  self.profile = {};
  self.profile.avatar = 'img/default-user.png';
  self.bioPlaceholder = "Renseignez votre profil et multipliez vos chances de trouver rapidement.";


  self.gotoBottom = () => {
    // set the location.hash to the id of
    // the element you wish to scroll to.
    console.log('gotoBottom');
    $location.hash('bottom');

    // call $anchorScroll()
    $anchorScroll();
    window.location.href = window.location.href
    let hrefSplited = window.location.href.split('#');
    setTimeout(()=> {
      window.location.href = hrefSplited[0] + "#" + hrefSplited[1];
    }, 500);
  };

  $scope.set_profile = () => { $log.debug('profile_editor.ctrl.js TODO profile update'); };

  $scope.snapshot = () => { $log.debug('profile_editor.ctrl.js TODO snapshot');
                            ImgSvc.do_camera('DATA_URL', 'CAMERA').then((url) => {
                              self.cropAvatar(url);
                            });
                            self.modal.hide();
                            self.modal.remove(); };

  $scope.galery = () => { $log.debug('profile_editor.ctrl.js TODO galery');
                          self.modal.hide();
                          ImgSvc.do_camera('DATA_URL', 'PHOTOLIBRARY').then((url) => {
                            self.cropAvatar(url);
                          });
                          self.modal.remove(); };

  $scope.cancel = () => { self.modal.hide();
                          $ionicLoading.hide();
                          self.modal.remove(); };


  self.cropAvatar = (url) => {
    $scope.image = "data:image/gif;base64," + url;
    $scope.myCroppedImage = { value: '' };
    ModalsSvc.cropImg($scope).then((modal) => {
      self.modal = modal;
    });
  }

  $scope.save = function(imgCroped) {
    $ionicLoading.show();
    FirebaseSvc.uploadBase64(imgCroped.value).then((snapshot) => {
      console.log("uploaded", snapshot);
      let url = _.get(snapshot, "metadata.downloadURLs[0]");
      if (url) {
        self.profile.avatar = url;
      }
      $scope.cancel();
      DataSvc.send('hm_client','set_profile',{'avatar':self.profile.avatar})
        .then(profile_response).catch(angular.noop);
    });
  }

  profile_response = (response) => {
    console.log("profile_response", response);
    $ionicLoading.hide();
  }

  self.editProfile = () => {
    console.log("profile", self.profile);
    $ionicLoading.show();
    DataSvc.send('hm_client','set_profile',{'surname':self.profile.surname,'name':self.profile.name, 'age':self.profile.age, 'activity': self.profile.activity, 'bio': self.profile.bio})
      .then(profile_response).catch(angular.noop);
  }

  self.set_avatar = () => {
    ModalsSvc.chooseMedia($scope).then((modal) => {
        self.modal = modal;
      });
  };

  self.init = () => {
    self.profile = SessionSvc.user_profile();
    if(!_.get(self.profile, 'avatar')) {
      self.profile.avatar = 'img/default-user.png';
    }
   };
  self.init();
};
