var ProfileCtrl = function($log,$scope,$state,$translate,SessionSvc, DataSvc) {
  let self = this;

  function init() {
                    let profile = angular.copy(SessionSvc.user_profile());
                    if(!angular.isDefined(profile.avatar))
                    {
                      profile.avatar = 'img/default-user.png';
                    }
                    self.full_name = [profile.firstname,profile.name].join(' ');
                    if(angular.isDefined(profile.age) || angular.isDefined(profile.activity))
                    {
                      self.is_user_info = true;
                      if(angular.isDefined(profile.age))
                      {
                         self.user_info = angular.isDefined(profile.activity)?
                         $translate.instant('profile.2',{age : profile.age}) + ', ' + profile.activity :
                         $translate.instant('profile.2',{age : profile.age});
                      }
                      else
                      {
                        self.user_info = profile.activity;
                      }
                    }
                    else
                    {
                      self.is_user_info = false;
                    }
                    self.profileProgress = 4;
                    self.is_user_bio = angular.isDefined(profile.bio);
                    self.is_complete_profile = ['age','activity','bio','email'].every(function(k) { return angular.isDefined(profile[k]); });
                    self.profile = profile;
                  };
  init();
};
