angular.module('profile',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.profile',
           { url : '/profile',
	           views : { 'navbar-view' : {
	                                       templateUrl: 'templates/profile/profile.html',
					                               controller: 'ProfileCtrl as ctrl'
                                       }
		                 }
           })
    .state('nav.profile_edit',
           { url : '/profile_edit',
	           views : { 'navbar-view' : {
	                                       templateUrl: 'templates/profile/profile_edit.html',
					                               controller: 'ProfileEditorCtrl as ctrl'
                                       }
		                 }
           });
})

.controller('ProfileCtrl',['$log','$scope','$state','$translate','SessionSvc', 'DataSvc', ProfileCtrl])
.controller('ProfileEditorCtrl',['$log','$scope','SessionSvc','ModalsSvc', '$location', '$anchorScroll','ImgSvc', 'FirebaseSvc', 'DataSvc', '$ionicLoading', ProfileEditorCtrl]);
