var DetailAddCtrl = function($log,$scope,$stateParams, FirebaseSvc, RENTING, $anchorScroll, $location, $timeout) {
//  $scope.add = $stateParams.args.add;
  let self = this;
  self.add = $stateParams.args;
  $log.info('detail_add.ctrl.js $stateParams : ' + JSON.stringify($stateParams));

  retrieveAvatar = () => {
    if (self.add && self.add.profile && self.add.profile.avatar) {
      self.avatar = self.add.profile.avatar;
    } else {
      self.avatar = 'img/default-user.png';
    }
  }
  resolveImgDownloadUrl = () => {
    if(self.add && self.add.images && self.add.images.length > 0) {
      FirebaseSvc.download(self.add.images)
        .then(images => {  self.images = [];
                           for(let i=0;i<images.length;i++) {
                             let o = images[i]; o.idx = i;
                             self.images.push(o);
                           }
                           console.log('self.images', self.images);
                        });
    } else {
       self.images = [{src: 'img/default-img.jpg'}];
    }
  }

  self.getImgSrc = (index) => {
    console.log("getImgSrc", index)
    return '';
  }

  self.getDateEnd = () => {
    if (self.add && self.add.period && self.add.period.duration) {
      let type = ''
      switch(self.add.period.period_type) {
        case 'month':
          type = 'months';
          break;
        case 'year':
          type = 'years';
          break;
      }
      return 'au ' + moment(self.add.from).add(self.add.period.duration, type).format('DD MMM');
    }
  }

  self.getRenting = () => {
    if(self.add) {
      let renting = self.add.renting;
      rentings = [];
    _.forEach(RENTING, (val, index) => {
      if (renting % val == 0) {
        rentings.push(index);
      }
    })
    return rentings.join();
    }
  }

  self.getIntent = () => {
    if (self.add && self.add.intent == 'open') {
      return 'A l’écoute du marché';
    } else {
      return 'En recherche active';
    }
  }


  self.getDuration = () => {
    console.log('self.add', self.add);
    let period = _.get(self, 'add.period');
    if (period && period.to) {
      let diff =  moment(period.to).diff(moment(period.from), 'months')
        return  (diff == 0 ? 1 : diff)+ ' mois';
    }
    return 'Période indéterminée';
  }

  self.options = {
    loop: false,
    effect: 'fade',
    speed: 500,
  }

  if (self.add) {
    resolveImgDownloadUrl();
    retrieveAvatar();
    self.intermediaries = [];
    _.forEach(self.add.intermediaries, (inter) => {
      self.intermediaries.push(inter.name + ' ' + inter.surname)
    });
    if (self.intermediaries.length == 1) {
      self.intermediariesCount = "1 relation"
    } else {
      self.intermediariesCount = self.intermediaries.length + "  relations"
    }
    self.intermediariesName = self.intermediaries.join(', ');
  }



let map_init = () => {
  if(angular.isDefined(self.add.for) && self.add.for == 'tenant')
                 {
                   let coords = [self.add.geo.coordinates.latitude,self.add.geo.coordinates.longitude],id = 'addMap';
                   let m = L.map(id,{zoomControl:false,
                                     boxZoom:false,
                                     doubleClickZoom:false,
                                     scrollWheelZoom:false,
                                     dragging:false }).setView(coords,12);
                   L.circle(coords,{weight:0,fillColor:'#30A75D',fillOpacity:0.3,radius:750,interactive:false}).addTo(m);
                   L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                   {
                     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                     subdomains: ['a','b','c']
                   }).addTo(m);
                 }
}

$timeout(map_init,2000);

};
