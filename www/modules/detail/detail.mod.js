/**
 * Module responsable de la gestion du detail.
 * @author Renaud Tripathi ganeshrd@laposte.net
 */
angular.module('detail',[])


.config( ($stateProvider) => {


  $stateProvider
    .state('detail_offer',
           { url : '/detail_offer',
            controller:  'DetailAddCtrl as ctrl' ,
	          templateUrl : 'templates/detail/detail_offer.html',
            params : { args : null },
            cache: false
          })
    .state('detail_request',
           { url : '/detail_request',
            controller:  'DetailAddCtrl as ctrl' ,
            templateUrl : 'templates/detail/detail_request.html',
            params : { args : null },
            cache: false
          })
})

.controller('DetailAddCtrl', ['$log','$scope','$stateParams', 'FirebaseSvc', 'RENTING', '$anchorScroll', '$location', '$timeout', DetailAddCtrl]);
