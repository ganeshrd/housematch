/**
 * Le main controller est référencé dans le template principale de navigation
 * i.e. www/templates/nav/nav.html
 */
var MainCtrl = function($log,$scope,$state) {
  $scope.state = $state;
  console.log('main.ctrl.js ' + $state.current.name);
};
