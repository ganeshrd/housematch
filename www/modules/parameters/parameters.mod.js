angular.module('parameters',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.parameters',
           { url : '/nav/parameters',
	     views : { 'navbar-view' : { 
	                                 templateUrl: 'templates/parameters/parameters.html', 
					 controller: 'ParametersCtrl as ctrl' 
				       } 
		     }
           });
})

.controller('ParametersCtrl',['$log','$scope',ParametersCtrl]);
