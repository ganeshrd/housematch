/**
 * Controleur de gestion du fil d'actualité.
 * Mise en forme des données pour affichage.
 * @todo La distance de relation doit reposer sur du svg dynamique
 * i.e. La distance doit être argument d'un script svg pour l'affichage
 * des 'pastilles' de relation'.
 */
var NewsCtrl = function($log,$scope,$state,$stateParams,$timeout,$location,$ionicScrollDelegate,DataSvc) {
  let self = this;
  $scope.data = {};

  self.types = {
    2 : true,
    3 : false,
    5 : false
  };
  self.adds;

  self.map = () => { $state.go('nav.news_map',{adds:self.adds}); };

  self.detail = (item,idx) => {
    if (item.for == "tenant") 
    {
      $state.go('detail_offer', {args:item});
    } 
    else 
    {
      $state.go('detail_request', {args:item});
    }
  }

  init = () => {
    DataSvc.send({action:'user_network'})
      .then(res => {
                     if(res.status == 'network')
                     {
                       self.adds = res.value;
                       let adds = angular.copy(self.adds);
                       adds = _.forEach(adds, (add,idx) => { 
                                                             if(add.distance > 3) 
                                                             { 
                                                               add.distance = 3; 
                                                             } 
                                                             let isSelected = function (type) { return !(add.renting % type); }; 
                                                             add.types = { 2 : isSelected(2), 3 : isSelected(3), 5 : isSelected(5) }; 
                                                             let meta = 1;
                                                             meta *= add.for == 'lessor'? 2 : 3;
                                                             meta *= angular.isDefined(add.profile.avatar)? 5 : 7;
                                                             add.meta = meta;
                                                           });
                       // add one or two advertisement
                       let l = adds.length;
                       if(l <= 5)
                       {
                         let i = Math.floor(l/2);
                         adds.splice(i,0,{addvert : true});
                       }
                       else
                       {
                         let i = Math.floor(l * 0.25);
                         let j = Math.floor(l * 0.75);
                         adds.splice(i,0,{addvert : true});
                         adds.splice(j,0,{addvert : true});
                       }
                       $scope.data.content = true;
                       $scope.data.items = adds;
                     }
                     else // status = no_network, value = []
                     {
                       $scope.data.content = false;
                     }
                   },
            err => { $log.error('news.ctrl.js error',err); $state.go('system_failure'); } )
            .then(function() { $timeout(function() { map_init();},0,false); })
            .then(function() { $timeout(function() { scroll();},0,false); });

  };

  function map_init() { 
    _.forEach($scope.data.items, 
              (add,idx) => { if(angular.isDefined(add.for))
                             {
                               let coords = [add.geo.coordinates.latitude,add.geo.coordinates.longitude],id = 'map'+idx;
                               let m = L.map(id,{zoomControl:false,
                                                 boxZoom:false,
                                                 doubleClickZoom:false,
                                                 scrollWheelZoom:false,
                                                 dragging:false }).setView(coords,12);
                               if(add.for == 'tenant')
                               {
                                 L.circle(coords,{weight:0,fillColor:'#30A75D',fillOpacity:0.3,radius:750,interactive:false}).addTo(m);
                               }
                               //L.tileLayer( 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png',  // humanitarian
                               L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',  // humanitarian
                               {
                                 attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                                 subdomains: ['a','b','c']
                                 // subdomains: ['a','b'] // humanitarian
                               }).addTo(m);
                             }
                           });
  };

  function scroll() { let arg = $stateParams.item;
                      let h = arg? 'hm' + arg.id + '-' + arg.timestamp : 'default';
                      $location.hash(h); 
                      $ionicScrollDelegate.anchorScroll(); };

  init();
};
