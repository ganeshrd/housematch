angular.module('news',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.news',
           { url : '/news',
             cache: false,
	           views : { 'navbar-view' : { templateUrl: 'templates/news/news.html', controller: 'NewsCtrl as ctrl' }},
             params : { item : null }
           })
    .state('nav.news_map',
           { url : '/news/map',
             views : { 'navbar-view' : { templateUrl : 'templates/news/map.html', controller : 'MapCtrl as ctrl' }},
             params : { adds : null }
           });
})

.controller('NewsCtrl',['$log','$scope','$state','$stateParams','$timeout','$location','$ionicScrollDelegate','DataSvc', NewsCtrl])
.controller('MapCtrl',['$log','$scope','$compile','$state','$stateParams','$filter','$translate', '$timeout', MapCtrl]);
