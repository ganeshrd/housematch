var MapCtrl = function($log,$scope,$compile,$state,$stateParams,$filter,$translate, $timeout) {
  var self = this;

  self.for_tenant_icon = L.icon({ iconUrl : 'img/icons/lessor-dot.svg', iconSize : [30,30] });
  self.for_lessor_icon = L.icon({ iconUrl : 'img/icons/tenant-dot.svg', iconSize : [30,30] });

  function init() {
    // prep data
    $scope.adds = $stateParams.adds.map(add => { let icon = (add.for == 'tenant')? self.for_tenant_icon : self.for_lessor_icon; 
                                                 let state = (add.for == 'tenant')? 'detail_offer' : 'detail_request';
                                                 return { 'add' : add, 'icon' : icon, 'state' : state}; });
    $log.debug('map.ctrl.js $scope.adds : ',$scope.adds);

    //L.tileLayer( 'http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', // humanitarian
    let tiles = L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                 {
                   maxZoom: 18,
                   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                   subdomains: ['a','b','c']
                   //subdomains: ['a','b'] // humanitarian
                 });
    let latLng = L.latLng(49.8153,6.1286);
    let m = L.map('main_map',{zoomControl:false, zoom:1, center:latLng, layers:[tiles]});

    let markers_for_tenant = L.markerClusterGroup({ iconCreateFunction: function(cluster) { return self.for_tenant_icon; },
                                                    disableClusteringAtZoom: 7 }); 
    let markers_for_lessor = L.markerClusterGroup({ iconCreateFunction: function(cluster) { return self.for_lessor_icon; },
                                                    disableClusteringAtZoom: 7 }); 

    let markerArray_for_tenant = [];
    let markerArray_for_lessor = [];
    for(let i = 0; i < $scope.adds.length; i++) { 
      let coord = $scope.adds[i].add.geo.coordinates; 
      let marker = L.marker(new L.latLng(coord.latitude,coord.longitude),{'icon' : $scope.adds[i].icon}); 
      marker.bindPopup(popup($scope.adds[i]),{closeButton:false,className:'hm-map-popup'}); 
      if($scope.adds[i].add.for == 'tenant')
      {
        markers_for_tenant.addLayer(marker); 
        markerArray_for_tenant.push(marker); 
      }
      else
      {
        markers_for_lessor.addLayer(marker); 
        markerArray_for_lessor.push(marker); 
      }
      if(i == $scope.adds.length -1) 
      { 
        let g1 = L.featureGroup(markerArray_for_tenant); 
        let g2 = L.featureGroup(markerArray_for_lessor); 
        m.fitBounds(g1.getBounds()); 
        m.fitBounds(g2.getBounds()); 
      } 
    }
  
    $scope.markers_for_tenant = markers_for_tenant;
    $scope.markers_for_lessor = markers_for_lessor;

    m.addLayer(markers_for_tenant);
    m.addLayer(markers_for_lessor);

    // add legend
    let legend = L.control({position:'topright'});
    legend.onAdd = function (map) {
                     let html = `<div class="info hm-map-legend">
                                   <div><img src="img/icons/lessor-dot.svg" ng-click="toggle('tenant',$event);"> ${$translate.instant('proposal')}</div>
                                   <div><img src="img/icons/tenant-dot.svg" ng-click="toggle('lessor',$event);"> ${$translate.instant('search')}</div>
                                 </div>`;
                     let fn = $compile(angular.element(html));
                     let scope = $scope.$new();
                     scope.map = m;
                     scope.markers_for_tenant = markers_for_tenant;
                     scope.markers_for_lessor = markers_for_lessor;
                     scope.visibility = { 'lessor' : false, 'tenant' : false };
                     scope.toggle = function(target,event) { event.stopPropagation(); 
                     let layer = target == 'tenant'? markers_for_tenant : markers_for_lessor;
                                                       if(scope.visibility[target])  map.removeLayer(layer);
                                                       else  map.addLayer(layer); 
                                                       scope.visibility[target] = !scope.visibility[target];
                                                      };
                                                         
                     let main_map = L.DomUtil.get('main_map');
                     let div = fn(scope)[0];
                     main_map.appendChild(div);
                     return div; 
                     //let div = L.DomUtil.create('div','info hm-map-legend',L.DomUtil.get('main_map'));
                     /**
                     div.innerHTML += `<div><img src="img/icons/lessor-dot.svg" onclick="alert('yooyou');"> ${$translate.instant('proposal')}</div>
                                       <div><img src="img/icons/tenant-dot.svg" onclick="alert('boob');"> ${$translate.instant('search')}</div>`;
                     $log.debug('map.ctrl.js',div.outerHTML);
                     */
                     };
    legend.addTo(m);
    $scope.map = m;
    $scope.map.on('click',center);
  };

  function popup(o) {
    let title = o.add.description.title, address = o.add.geo.address, goods = o.add.goods;
    let charge = o.add.charge, renting = o.add.renting, period = o.add.period;
    let state = o.state;
    let html = `<div hm-el="popup" ui-sref="${state}({ args : add })">
                  <hm-map-popup ng-model="add"></hm-static-renting-bar>
                  <div hm-el="header">${title}-${address} </div>
                  <div hm-el="body">
                    <div hm-el="body-left"> <div>${$filter('hm_goods')(goods)}</div> <div>${$filter('hm_period')(period)}</div> </div>
                    <div hm-el="body-right"> <div>${$filter('hm_charge')(charge)}</div> <div>${$filter('renting')(renting)}</div> </div>
                  </div>
                </div>`;
     $log.debug('map.ctrl.js html : ',html);
     let fn = $compile(angular.element(html));
     scope = $scope.$new();
     scope.add = o.add;
     $log.debug('map.ctrl.js fn : ',fn);
     let popup = new L.popup().setContent(fn(scope)[0]);

     return popup;
  };

  function center(e) { $scope.map.setView(e.latlng); };

  $timeout(init, 2000);
};
