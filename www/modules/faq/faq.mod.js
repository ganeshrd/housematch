angular.module('faq',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.faq',
           { url : '/nav/faq',
	     views : { 'navbar-view' : { 
	                                 templateUrl: 'templates/faq/faq.html', 
					 controller: 'FaqCtrl as ctrl' 
				       } 
		     }
           });
})

.controller('FaqCtrl',['$log','$scope',FaqCtrl]);
