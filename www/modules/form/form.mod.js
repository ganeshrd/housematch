angular.module('form',['angular-md5','hm_filters'])

.config(function($stateProvider) {
  $stateProvider
    .state('form_create',
      {
        url: '/form/create',
        params : { form_for : "lessor" },
        cache : false,
        templateUrl: 'templates/form/create.html',
        controller : 'FormCtrl as ctrl'
    })
    .state('form_edit',
      {
        url: '/form/edit',
        params : { add : null }, 
        cache : false,
        templateUrl: 'templates/form/edit.html',
        controller : 'EditFormCtrl as ctrl'
    })
})
.controller('FormCtrl',['$log','$scope','$state','$stateParams','$translate', 'DataSvc','FormsSvc','SessionSvc','DatepickerSvc',FormCtrl])
.controller('EditFormCtrl',['$log','$scope','$state','$stateParams','$translate', 'DataSvc','SessionSvc','DatepickerSvc','FormsSvc',EditFormCtrl]);
