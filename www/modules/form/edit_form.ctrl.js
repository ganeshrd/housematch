/**
 * Controleur du formulaire d'annonce.
 * Dans un cas optimum, le contrôleur ne sera chargé qu'une seule fois.
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 */
var EditFormCtrl = function($log,$scope,$state,$stateParams,$translate, DataSvc,SessionSvc,DatepickerSvc,FormsSvc) {
  let self = this;

  function init() {
    $scope.validation = { btn_disabled : false };
    $scope.add = $stateParams.add;
  	self.step = 1;
    if($scope.add.for == 'tenant') 
    {
      self.title = $translate.instant('form-for-tenant/title');
      self.subTitle = $translate.instant('form-for-tenant/renting-tip');
      self.stepCount = 3;
    } 
    else // lessor is default
    {
      self.title = $translate.instant('form-for-lessor/title');
      self.subTitle = $translate.instant('form-for-lessor/renting-tip');
      self.stepCount = 2;
      $scope.add.for = 'lessor';
    }
  
    $scope.datepickerObject = DatepickerSvc.params();
  
    let init_dates = [ new Date($scope.add.period.from * 1000) ];
    if($scope.add.period.timeframe) init_dates.push(new Date($scope.add.period.to * 1000));
    $scope.selectedDates = angular.copy(init_dates); 
    $scope.no_timeframe = !$scope.add.period.timeframe;
  
    $scope.datepickerObject.selectedDates = $scope.selectedDates;
    $scope.datepickerObject.callback = function (dates, period) { retSelectedDates(dates, period); }
  };

  let retSelectedDates = function (dates, no_timeframe) {
    $scope.selectedDates.length = 0;
    for (var i = 0; i < dates.length; i++) {
      $scope.selectedDates.push(angular.copy(dates[i]));
    }
    $scope.no_timeframe = no_timeframe;
  };

 $scope.gotToNews = (modal) => {
	 $state.go('nav.news');
	 modal.hide();
 }
 $scope.gotToMyAdds = (modal) => {
	 $state.go('nav.adds');
	modal.hide();
 }

	self.next = () => {
		if(self.step == self.stepCount) {
			self.update();
			return;
		}
		self.step++;
	}
  /**
   * Envoi de l'annonce de l'utilisateur
   */
  self.update = () => {
    $scope.validation.btn_disabled = true;
    let unix = Object.values($scope.selectedDates).map( d => { return Math.round(d.getTime()/1000); });
    let booktime = { from : Math.min.apply(Math,unix),
                     timeframe : (!$scope.no_timeframe).toString(),
                   };
    if(booktime.timeframe) booktime.to = Math.max.apply(Math,unix); 
    $scope.add.period = booktime;
    FormsSvc.update($scope.add)
      .then(resp1 => { let mandatories = { action : 'user_state' }; 
                       DataSvc.send(mandatories)
                         .then(resp2 => { SessionSvc.set_user_state(resp2.value); 
                                          $state.go('nav.news'); }); }).catch(err => { $log.error('edit_form.ctrl.js err',err); $state.go('system_failure'); }); };

  init();
};
