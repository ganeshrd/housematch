/**
 * Controleur du formulaire d'annonce.
 * Dans un cas optimum, le contrôleur ne sera chargé qu'une seule fois.
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 */
var FormCtrl = function($log,$scope,$state,$stateParams,$translate, DataSvc,FormsSvc,SessionSvc,DatepickerSvc) {
  let self = this;

  function init() {
    $scope.validation = { btn_disabled : false };
  	self.step = 1;
    self.stepCount = 2;
    self.form_for = $stateParams.form_for;
    $scope.add = FormsSvc.new_form(self.form_for);
    if(self.form_for == 'tenant') 
    {
      self.title = $translate.instant('form-for-tenant/title');
      self.subTitle = $translate.instant('form-for-tenant/renting-tip');
      self.stepCount = 3;
    } 
    else // lessor is default
    {
      self.title = $translate.instant('form-for-lessor/title');
      self.subTitle = $translate.instant('form-for-lessor/renting-tip');
      self.stepCount = 2;
      $scope.add.for = 'lessor';
    }
  
    $scope.no_timeframe = true;
    $scope.datepickerObject = DatepickerSvc.params();
    $scope.selectedDates = [];
    $scope.datepickerObject.selectedDates = $scope.selectedDates;
    $scope.datepickerObject.callback = function (dates, period) { retSelectedDates(dates, period); }
  };

  function retSelectedDates(dates, no_timeframe) {
    $scope.selectedDates.length = 0;
    for (var i = 0; i < dates.length; i++) {
      $scope.selectedDates.push(angular.copy(dates[i]));
    }
    $scope.no_timeframe = no_timeframe;
  };

 $scope.gotToNews = (modal) => {
	 $state.go('nav.news');
	 modal.hide();
 }
 $scope.gotToMyAdds = (modal) => {
	 $state.go('nav.adds');
	modal.hide();
 }

	self.next = () => {
		if(self.step == self.stepCount) {
			self.publish();
			return;
		}
		self.step++;
	}
  /**
   * Envoi de l'annonce de l'utilisateur
   */
  self.publish = () => {
    $scope.validation.btn_disabled = true;
    let unix = Object.values($scope.selectedDates).map( d => { return Math.round(d.getTime()/1000); });
    let booktime = { from : Math.min.apply(Math,unix),
                     timeframe : (!$scope.no_timeframe).toString(),
                   };
    if(booktime.timeframe) booktime.to = Math.max.apply(Math,unix);
    $scope.add.period = booktime;
    FormsSvc.publish($scope.add)
      .then(resp1 => { $log.debug('form.ctrl.js resp1',resp1);
                       let mandatories = { action : 'user_state' }; 
                       DataSvc.send(mandatories)
                         .then(resp2 => { $log.debug('form.ctrl.js resp2',resp2);
                                          SessionSvc.set_user_state(resp2.value); 
                                          $state.go('nav.news'); }); }).catch(err => { $state.go('system_failure'); }); };

  init();
};
