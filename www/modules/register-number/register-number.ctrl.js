/**
 * Enregistrer le numéro d’enregistrement en ligne.
 *
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var RegisterNumberCtrl = function($log,$scope,$state,DataSvc, $stateParams) {
  var self = this;
  self.step = 1;

  self.id_add = $stateParams.id_add;

  self.submitRegisterNumber = () => {
    let data = {id_add: self.id_add, add_state:{'status': "processing", 'data': self.registerNumber }};
    DataSvc.send('hm_client','update_add', data)
      .then(r => { $state.go('register-number-success');}, e => {console.error('repeater.ctrl.js ' + e);}).catch(angular.noop);
  }

  self.accept = () => {
    self.step = 2;
  }

  self.validate = () => {
    let selected = _.find(self.user_adds, (add) => {
       return add.selected
    });
    if(selected) {
      self.selected = selected;
      self.step = 3;
    }
  }

  self.canValidate = () => {
    let selected =_.find(self.user_adds, (add) => {
       return add.selected
    });
    return !selected;
  }


  self.back = () => {
    $state.go('register-number')
    console.log('back');
  }

  self.validatePermission = () => {
    if (self.acceptPermission) {
      let selected =_.find(self.user_adds, (add) => {
         return add.selected
      });
      console.log('id_add', self.id_add);
      $state.go('register-number-form', {id_add: self.id_add});
    } else {
      $state.go('register-number-infos')
    }
  }

  self.next = () => {
    if (self.isPrincipalResidence == true || self.isPrincipalResidence == false) {
      self.step = 4;
    }
  };

  self.submit = () => {
    console.log('submit');
    if (self.isPrincipalResidence != true && self.isPrincipalResidence != false) {
      return;
    }
    self.selected  = _.find(self.user_adds, (add) => {
       return add.selected
    });
    switch (self.isPrincipalResidence) {
      case true:
        if(self.isBigCity) {
          $state.go('register-number-permission', {id_add: self.selected['_id']});
        } else {
          let data = {id_add: self.selected['_id'], add_state:{'status': "clear" }};
          DataSvc.send('hm_client','update_add', data)
            .then(r => { $state.go('register-number-success');}, e => {console.error('repeater.ctrl.js ' + e);}).catch(angular.noop);
        }
        break;
      case false:
          if(self.isBigCity) {
            $state.go('register-number-submit');
          } else {
            let data = {id_add: self.selected['_id'], add_state:{'status': "clear" }};
            DataSvc.send('hm_client','update_add', data)
              .then(r => { $state.go('register-number-success');}, e => {console.error('repeater.ctrl.js ' + e);}).catch(angular.noop);
          }
          break;

    }
  }

  self.choose = (add) => {
    if(add.treated) {
      return;
    }
    let isSelected = add.selected;
    _.forEach(self.user_adds, (add) => {
      add.selected  = false;
    });
    add.selected = !isSelected;
  }


  DataSvc.send('hm_client','get_adds').then((response) => {
    self.user_adds = [];
    _.forEach(response.data, add => {
      if (add.for == "tenant") {
        add.treated = !!(add['add_state'])
        self.user_adds.push(add);
      }
    })
    console.log('response', response.data);

  })
};
