var RegisterNumberPermissionCtrl = function($log,$scope,$state,DataSvc, $stateParams) {
  var self = this;

  self.accept;
  self.btn_disabled = true;
  self.id_add = $stateParams.id_add;

  $log.debug('register-number-permisson.ctrl.js params', $stateParams);

  self.validate = () => {
    if (self.accept)
    {
      $log.debug('id_add', self.id_add);
      $state.go('register-number-form', {id_add: self.id_add});
    }
    else
    {
      $state.go('register-number-infos');
    }
  };
};
