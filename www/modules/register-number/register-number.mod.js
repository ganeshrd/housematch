angular.module('register-number',['angular-md5','hm_filters'])

.config(function($stateProvider) {
  $stateProvider

    .state('register-number',
           { url: '/register-number',
             templateUrl: 'templates/register-number/register-number.html',
             controller: 'RegisterNumberCtrl as ctrl'
            })
    .state('register-number-permission',
           { url: '/register-number-permission',
             templateUrl: 'templates/register-number/register-number-permission.html',
             controller: 'RegisterNumberPermissionCtrl as ctrl',
             params: {id_add: ''}
            })
    .state('register-number-form',
           { url: '/register-number-form',
             templateUrl: 'templates/register-number/register-number-form.html',
             controller: 'RegisterNumberFromCtrl as ctrl',
             params: {id_add: ''}
            })
    .state('register-number-success',
           { url: '/register-number-success',
             templateUrl: 'templates/register-number/register-number-success.html'
            })
    .state('register-number-infos',
           { url: '/register-number-infos',
             templateUrl: 'templates/register-number/register-number-infos.html'
            })
    .state('register-number-submit',
           { url: '/register-number-submit',
             templateUrl: 'templates/register-number/register-number-submit.html',
             controller: 'RegisterNumberCtrl as ctrl'
            })
})
.controller('RegisterNumberCtrl',['$log','$scope','$state','DataSvc', '$stateParams', RegisterNumberCtrl])
.controller('RegisterNumberFromCtrl',['$log','$scope','$state','DataSvc', '$stateParams', RegisterNumberFromCtrl])
.controller('RegisterNumberPermissionCtrl',['$log','$scope','$state','DataSvc', '$stateParams', RegisterNumberPermissionCtrl]);
