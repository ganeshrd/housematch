/**
 * Enregostrer le numéro d’enregistrement en ligne.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var RegisterNumberFromCtrl = function($log,$scope,$state,DataSvc, $stateParams) {
  var self = this;
  self.step = 1;
  self.id_add = $stateParams.id_add;
  $log.debug('register-number-form.ctrl.js', $stateParams);
  $log.debug('self.id_add', self.id_add);
  self.bindings = {
    name:"",
    surname:"",
    email: "",
    city:"",
    code:"",
    address:""
  }


  let validEmail = (e) => {
  var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
  return String(e).search (filter) != -1;
  }

  self.submitRegisterNumber = () => {
    console.log('submitRegisterNumber', self.registerNumber);
    $state.go('register-number-success');
  }

  self.canDoNext = () => {
    switch(self.step) {
      case 1:
        if(self.bindings.name && self.bindings.surname && validEmail(self.bindings.email)) {
          return true;
        }
        return false;
      case 2:
        if(self.bindings.address && self.bindings.city && self.bindings.code) {
          return true;
        }
        return false;
      case 3:
        if(self.bindings.floor && self.bindings.beds && self.bindings.rooms) {
          return true;
        }
        return false;
      }
  }

  self.next = () => {
    if (!self.canDoNext()) {
      return;
    }
    if(self.step == 3) {
      let o = {id_add:self.id_add, add_state: { data : self.bindings, 'status' : 'processing' }};
      $log.debug('register-number-form.ctrl ', o);
      DataSvc.send('hm_client','update_add', o)
      .then(() => {
        $state.go('register-number-success');
      });
    } else {
      self.step++;
    }

    console.log('next', self.step);
  }

  self.back = () => {
    console.log('back', self.step);
    if (self.step == 1) {
      $state.go('register-number-permission');
      return;
    }
    self.step--;
  }


};
