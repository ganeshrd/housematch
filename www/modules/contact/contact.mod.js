angular.module('contact',[])

.config( ($stateProvider) => {
	$stateProvider
	.state('nav.contact',
		{ url : '/nav/contact',
		views : { 'navbar-view' : { 
			templateUrl: 'templates/contact/contact.html', 
			controller: 'ContactCtrl as ctrl' 
		} 
	}
});
})

.controller('ContactCtrl',['$log','$scope','$ionicModal','ModalsSvc',ContactCtrl]);
