angular.module('exit',[])

.config(function($stateProvider) {
  $stateProvider
    
    .state('network_failure',
           { url : '/exit/network_failure',
	           templateUrl: 'templates/exit/network_failure.html' })

    .state('user_failure',
           { url : '/exit/user_failure',
	           templateUrl: 'templates/exit/user_failure.html',
             controller : 'UserExitCtrl as ctrl' })

    .state('system_failure',
           { url : '/exit/system_failure',
	           templateUrl: 'templates/exit/system_failure.html',
             controller : 'SystemExitCtrl as ctrl' });

 })

 .controller('UserExitCtrl',['$log','$scope',UserExitCtrl])
 .controller('SystemExitCtrl',['$log','$scope',SystemExitCtrl]);
