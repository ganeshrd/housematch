/**
 * Controleur de sortie d'application pour
 * les erreurs système.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */

var SystemExitCtrl = function($log,$scope) {
  $scope.exit = () => { $log.warn('system_exit.ctrl.js system error'); ionic.Platform.exitApp(); };
};
