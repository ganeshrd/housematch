var AddEditCtrl = function($log,$scope,$state,$stateParams,$translate,FormsSvc,DataSvc,SessionSvc,FirebaseSvc,ModalsSvc) {
  let self = this;

  self.tmp = undefined;
  self.add = $stateParams.args;
  console.log("$stateParams.args",$stateParams.args)
  self.firebase = true; // availability flag

  $scope.title = self.add.for == 'tenant'? $translate.instant("adds.0") : $translate.instant("adds.1");
  $scope.add = angular.copy(self.add);
  $log.debug('add_edit.ctrl.js ' + hm.dbg_object($scope.add));

  /**
   * Téléchargement des images de l'annonce.
   * En cas de succès affecte la variable d'instance self.images.
   * En cas d'échec, un traitement conditionnel à l'utilisateur est appliqué.
   * @param retry Boolean Si vrai, il s'agit d'une nième tentative (n > 0).
   */
  self.resolve_firebase_refs = function(retry) {
    if(retry != true)
    {
      self.tmp = angular.copy(self.add);
    }
    if(!self.add.images.length)
    { // no images yet
      console.log('add_edit.ctrl.js no images'); $scope.add = self.tmp; return;
    }
    FirebaseSvc.download(self.add.images)
      .then(images => {  self.tmp.images = [];
                         for(let i=0;i<images.length;i++) {
                           let o = images[i]; o.idx = i;
                           self.tmp.images.push(o);
                         }
						             $scope.add = self.tmp;
                         // $log.debug('add_edit.ctrl.js ' + hm.dbg_object($scope.add.images));
                      },
	             err => {
                        ModalsSvc.firebase_failure()
                          .then(resp => {
                                          switch(resp) {
                                            case 'exit' : $log.warning('add_edit.ctrl.js user_failure');
                                                          $state.go('user_failure');
                                                          break;
                                            case 'retry' : $log.info('add_edit.ctrl.js retry');
                                                           self.resolve_firebase_refs(true);
                                                           break;
                                            case 'skip' : log.warn('add_edit.ctrl.js editing without firebase service');
                                                          self.firebase = false;
                                                          return;
                                                          break;
                                            default : $log.error('add_edit.ctrl.js unexpected case ' + resp);
                                                      $state.go('system_failure');
                                          }
                                        }
 	                       );
 		                 });
  };
  // init
  self.resolve_firebase_refs(); // resolve images and assign $scope.add

  /**
   * Conpare deux objets a et b puis retourne un objet
   * dont les propriétés sont celle de a qui sont différentes de
   * celles de b.
   */
  self.odiff = (a,b) => { return _.reduce(a,function(r,v,k) { if(_.isEqual(v,b[k])) return r;
							      let entry = {}; entry[k] = v;
							      return Object.assign(r,entry);
							     },{}); };

  /**
   * Mise à jour de firebase storage
   * @param deleted Array Les références supprimées de l'annonce.
   * @param data_uris Array Les nouvelles images de l'annonce au format base64.
   * @param update L'ensemble des données misent à jour.
   */
  self.storage_update = function(deleted,data_uris,update) {
    FirebaseSvc.update(deleted,data_uris)
	    .then(images => { update.images = images;
	                      self.send_update(update); },
	          err    => { ModalsSvc.firebase_failure()
                          .then(resp => {
                                          switch(resp) {
                                            case 'exit' : $log.warning('add_edit.ctrl.js user_failure');
                                                          $state.go('user_failure');
                                                          break;
                                            case 'retry' : $log.info('add_edit.ctrl.js retry');
                                                           self.storage_update(deleted,data_uris,update);
                                                           break;
                                            case 'skip' : log.warn('add_edit.ctrl.js editing without firebase service');
                                                          self.firebase = false;
                                                          delete update.images;
                                                          if(!_.isEmpty(update))
                                                          {
                                                            self.send_update(update);
                                                          }
                                                          else
                                                          {
                                                            $log.warn('add_edit.ctrl.js firebase failure and nothhing to update');
                                                            $state.go('nav.news');
                                                          }
                                                          break;
                                            default : $log.error('add_edit.ctrl.js unexpected case ' + resp);
                                                      $state.go('system_failure');
                                          }
                      }); });
  };

  /**
   * Controle le changement d'état de l'annonce.
   * Met à jour l'annonce conditionnellement.
   */
  self.update = function() {
    let update = self.odiff($scope.add,self.add);
    if(!_.isEmpty(update))
    {
      Object.assign(update,{id_add:self.add._id}); // hack for server bug see #server-1
      if(('images' in update) && self.firebase)
      {
        // search for deleted images
	      let deleted = self.add.images.filter(src => { return update.images.indexOf(src) == -1; });
	      // search new adds (data uri)
	      let data_uris = update.images.filter(data => { return self.add.images.indexOf(data) == -1; });
	      self.storage_update(deleted,data_uris,update);
      }
      else self.send_update(update);
    }
    else $state.go('nav.news');
  };

  /**
   * Mise à jour de l'annonce
   * @param data Object les données modifiées.
   */
  self.send_update = function(data) {
    FormsSvc.update(data).then(r => { self.update_state(); }).catch(angular.noop);
  };

  /**
   * Mise à jour de l'état de l'utilisateur
   */
  self.update_state = function() {
    DataSvc.send('hm_client','user_state').then(s => { SessionSvc.set_user_state(s.data);
                                                       $state.go('nav.news'); }).catch(angular.noop);
  };
};
