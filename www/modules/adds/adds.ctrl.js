/**
 * HouseMatch
 * controleur de gestion des annonces de l'utilisateur
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var AddsCtrl = function($log,$scope,$state,$translate,SessionSvc,ModalsSvc,DataSvc,SessionSvc) {
  var self = this;

  $scope.edit = function(add) { $scope.modal.hide(); $scope.modal.remove();
                                $log.debug('adds.ctrl.js add',add);
                                $state.go('form_edit',{ 'add' : add }); };

  $scope.delete = function(add) { ModalsSvc.hm_confirm({ template : $translate.instant('modals/confirm/delete'),
                                                         okText : $translate.instant('btn_validate'),
							                                           cancelText : $translate.instant('btn_cancel') })
				                            .then(resp => { if(resp) 
                                                    {
                                                      DataSvc.send({'action':'delete_add'},{'timestamp':add.timestamp})
                                                        .then( resp => { $log.info('adds.ctrl.js delete remaing adds', resp.value);
						                                                             $scope.modal.hide(); $scope.modal.remove();
                                                                         SessionSvc.refresh().then(ok => { $scope.adds = SessionSvc.user_adds(); }); });
                                                    }
				                                            else
						                                        {
						                                          $scope.modal.hide(); $scope.modal.remove();
						                                        } })
                                    .catch(err => { $log.error('adds.ctrl.js error',err); $state.go('system_failure'); }) };

  $scope.cancel = () => { $scope.modal.hide(); $scope.modal.remove(); };

  /**
   * triggered on a add basis in the template
   */
  $scope.select = function(add) { $scope.add = add; ModalsSvc.adds_edit($scope).then(modal => { $scope.modal = modal; $scope.modal.show(); }); };

  function init() { $scope.adds = SessionSvc.user_adds();
                    $log.debug('adds.ctrl.js $scope.adds',$scope.adds); };

  init();
};
