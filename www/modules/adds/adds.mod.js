angular.module('adds',[])

.config( $stateProvider => {
  $stateProvider
    .state('nav.adds',
           { url : '/adds',
             cache : false,
	           views : { 'navbar-view' : { templateUrl: 'templates/adds/adds.html', controller: 'AddsCtrl as ctrl' } }
	         });
})

.controller('AddsCtrl',['$log','$scope','$state','$translate','SessionSvc','ModalsSvc','DataSvc','SessionSvc',AddsCtrl]);
