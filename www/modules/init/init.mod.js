/**
 * @module Init module
 * Rassemble les décalarations des controleurs nécéssaire à la
 * phase préliminaire de l'inscription d'un utilisateur.
 * Ce module ne doit être solicité qu'une seule fois dans la
 * vie normale de l'application.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
angular.module('init',['hm_filters'])

.factory('InitSvc',InitSvc)
.config(function($stateProvider) {
  $stateProvider
    .state('init',
      { url: '/init',
        template: '<img src="img/loading.png"/>'
      })
    .state('tutorial',
           { url: '/tutorial',
            templateUrl: 'templates/init/tutorial.html',
             controller: 'TutorialCtrl as ctrl'
    })
})
.controller('TutorialCtrl',['$log','$scope', '$rootScope', '$state','$q','$ionicModal','$ionicSlideBoxDelegate',
                            'InitSvc','UsersSvc', 'RegisterSvc', 'KvsSvc', 'ImgSvc','SessionSvc', 'ModalsSvc', TutorialCtrl]);
