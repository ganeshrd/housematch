/**
 * @file tutorial.ctrl.js
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */

/**
 * Controleur du module init. Intervient dans la phase préliminaire de
 * l'inscription d'un utilisateur.
 */
var TutorialCtrl = function( $log,$scope,$rootScope,$state,$q,$ionicModal,$ionicSlideBoxDelegate,
                             InitSvc,UsersSvc,RegisterSvc,KvsSvc,ImgSvc,SessionSvc,ModalsSvc) {
  var self = this;

  self.topics = InitSvc.topics();
  self.add = InitSvc.add();
  self.add2 = InitSvc.add2();
  self.add3 = InitSvc.add3();
  self.step = 1;
  self.next = next;
  self.skip = skip;
  self.admin_menu = admin_menu;
  self.logIn = logIn;
  self.tap_count = 0;
  self.swiperOptions = {
   /* Whatever options */
   effect: 'slide',
   initialSlide: 0,
   /* Initialize a scope variable with the swiper */
   onInit: function(swiper){ self.swiper = swiper; },
     onSlideChangeEnd: function(swiper){ self.swiper.activeIndex = swiper.activeIndex; }
  };

  function next() {
    if(self.swiper.activeIndex == 3)
    {
      skip();
    }
    else
    {
      self.swiper.slideNext();
    }
  }

  function skip() {
    $state.go("register.comment");
  }

  function loadUserAndDisplayUsers() {

    UsersSvc.get_users().then( (resp) => {
      self.users = resp.value;
      self.modal.show();
    });
  }

  $ionicModal.fromTemplateUrl('templates/init/displayUsersModal.html', {
    scope: $scope,
    animation: 'slide-in-down'
  }).then( modal => {
    self.modal = modal;
  });

  function logIn(user) {
      KvsSvc.store_object({id:user.id,pwd:user.pwd})
        .then(r => { SessionSvc.set_user_state(user);
                     self.modal.hide();
                     $state.go('nav.news'); },

              e => { self.modal.hide();
                     $log.error('tutorial.ctrl.js logIn failure',e);
                     $state.go('system_failure'); });
  };

  function admin_menu() {
    self.tap_count++;
    if(self.tap_count >= 3) {
      loadUserAndDisplayUsers()
    }
  };

  self.aboutHM = () => {
    // ModalsSvc.displayAboutHM();
    $state.go("register.comment");
  }

  $scope.goToFirstSlide = function(){
   $scope.swiper.slideTo(0);
 };
};
