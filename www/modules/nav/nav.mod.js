/**
 * Module responsable de la gestion de l'affichage des vues avec barre de navigation.
 * @author Renaud Tripathi ganeshrd@laposte.net
 */
angular.module('nav',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav',
           { url : '/nav',
	           abstract : true,
	           templateUrl: 'templates/nav/nav.html' });
})

.controller('NavCtrl',['$log',NavCtrl]);

