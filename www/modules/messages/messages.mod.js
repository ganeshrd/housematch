angular.module('messages',[])

.config( ($stateProvider) => {
  $stateProvider
    .state('nav.messages',
           { url : '/messages',
             views : { 'navbar-view' : { templateUrl: 'templates/messages/messages.html', controller: 'MessagesCtrl as ctrl' } }
           })
     .state('nav.message',
            { url : '/message',
              params : { tel : null, room: null },
              cache: false,
              views : { 'navbar-view' : { templateUrl: 'templates/messages/message.html', controller: 'MessageCtrl as ctrl' } }
            });
})

.controller('MessagesCtrl',['$log','$scope','SessionSvc', 'MessageSvc', 'FirebaseSvc', MessagesCtrl])
.controller('MessageCtrl',['$log','$scope','SessionSvc', '$stateParams', 'MessageSvc', 'FirebaseSvc', MessageCtrl]);
