var MessageCtrl = function($log,$scope,SessionSvc, $stateParams, MessageSvc, FirebaseSvc) {
  $scope.title = "Message";

  let vm = this;
  vm.textAreaBottom = '50px';
  console.log('user_profile()', SessionSvc.user_profile());
  console.log('$stateParams', $stateParams);
  vm.mail_receiver = $stateParams.email_receiver;
  vm.room = {
    message: []
  };
  vm.onGoing = true;
  vm.textAreaheight = 42;
  vm.getRoomHeight = () => {
    return "calc(100vh - 17em - "  + (vm.textAreaheight - 42) + "px)";
  };
  var textarea = document.querySelector('textarea');

  let autosize = () => {
    var el = this;
    var elem = angular.element(document.querySelector(".textar"));
      // el.style.cssText = 'height:auto; padding:0';
      // for box-sizing other than "content-box" use:
      // el.style.cssText = '-moz-box-sizing:content-box';
      // console.log('el.scrollHeight', el.scrollHeight);
      // el.style.cssText = 'height:' + (el.scrollHeight + 10) + 'px';
  };

  textarea.addEventListener('keydown', () => {
    var el = this;
    var elem = angular.element(textarea);

    if (elem && elem.length > 0) {
      let el = elem[0];
      el.style.cssText = 'height:auto; padding:0';
      el.style.cssText = 'height:' + (el.scrollHeight + 10) + 'px';
      vm.textAreaheight = (el.scrollHeight);
    }
    // el.style.cssText = 'height:auto; padding:0';
    // for box-sizing other than "content-box" use:
    // el.style.cssText = '-moz-box-sizing:content-box';
    // console.log('el.scrollHeight', el.scrollHeight);
  });

  let refreshScroll = () => {
      let chat = document.getElementById("chat-room");
      chat.scrollTop = chat.scrollHeight;
      if (!$scope.$$phase) $scope.$apply();
  }

  window.addEventListener('keyboardDidShow', (event) => {
    // Describe your logic which will be run each time when keyboard is about to be shown.
  });

  window.addEventListener('keyboardHeightWillChange', function (event) {
    // Describe your logic which will be run each time when keyboard is about to be closed.
    event.keyboardHeight = event.keyboardHeight < 50 ? 50 : event.keyboardHeight;
    vm.textAreaBottom = event.keyboardHeight+'px';
  });

  vm.send = () => {
    if(vm.onGoing) {
      return;
    }
    MessageSvc.addMessage(vm.room.key, vm.theMessage).then((val)=> {
      vm.theMessage = "";
      refreshScroll();
    });
  };


  vm.getMessages = () => {
    return vm.room.messages;
  };

  vm.isEmptyMessage = () => {
    return !vm.error && !vm.onGoing && (!vm.room.messages || vm.room.messages.length == 0);
  };

  let init = () => {
    vm.userUid = FirebaseSvc.uid();
    if($stateParams.tel) {
      vm.onGoing = true;
      MessageSvc.getRoom($stateParams.tel).then((room)=> {
        vm.room = room;
        console.log('room --- room', room);
        vm.messages = vm.room.messages || [];
        vm.onGoing = false;
        setTimeout(() => {
          refreshScroll();
        }, 1000);
        obServeChat();
      }, (error) => {
        vm.onGoing = false;
        vm.error = true;
      });
    } else if($stateParams.room) {
      vm.room = $stateParams.room;
      obServeChat();
    }

  };

  let obServeChat = () => {
    vm.onGoing = true;
    firebase.database().ref().child('rooms/' + vm.room.key).on('value', snapshot => {
      let userUid = FirebaseSvc.uid();
      let room = snapshot.val();
      vm.onGoing = false;
      let members = _.get(vm.room, 'members');
      _.forEach(members, (member, key) => {
        if (key != userUid) {
          vm.room.title = member.surname + '  ' + member.name;
        }
      });
      vm.room.messages = room.messages || [];
      if (!$scope.$$phase) $scope.$apply();
      refreshScroll();
    });
  }

  setTimeout(() => {
    init();
  }, 2000)
};
