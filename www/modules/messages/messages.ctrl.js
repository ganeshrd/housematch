var MessagesCtrl = function($log,$scope,SessionSvc, MessageSvc, FirebaseSvc) {
  $scope.title = "Messages";
  let vm = this;
  self.init = () => {
    MessageSvc.getMessages().then((rooms) => {
      let userUid = FirebaseSvc.uid();
      _.forEach(rooms, room => {
        console.log('rooms', rooms);
        if (room.messages && room.messages.length > 0) {
          let members = _.get(room, 'members');
          _.forEach(members, (member, key) => {
            if (key != userUid) {
              room.title = member.surname + '  ' + member.name;
            }
          });
          room.content = _.last(room.messages).content;
        };
    });
      vm.rooms = rooms;
    });
  };

  self.init();
};
