angular.module('tests',[])

.config(function($stateProvider) {
  $stateProvider
    .state('test',
           { url : '/test',
             abstract : true,
             templateUrl : 'templates/tests/test.html' })
    .state('test.zombie',
            { url : '/nav/zombie',
	            views : { 'test-view' : { templateUrl: 'templates/tests/zombie.html', controller: 'ZombieCtrl as ctrl' } }
            })
    .state('test.db_reset',
            { url : '/nav/db_reset',
	            views : { 'test-view' : { templateUrl: 'templates/tests/db_reset.html', controller: 'ResetCtrl as ctrl' } }
            });
})
.controller('ZombieCtrl',['$log','$scope','$state','KvsSvc','DataSvc','SessionSvc','UsersSvc',ZombieCtrl])
.controller('ResetCtrl',['$log','$scope','$state','$ionicLoading','DataSvc','SessionSvc','KvsSvc',ResetCtrl]);
