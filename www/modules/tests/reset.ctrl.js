/**
 * TEST ONLY
 * Ce controleur doit êter supprimer de la version de production.
 * Supprime toute les entrées de la db serveur et relance le 
 * script de peuplement de la base du serveur.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var ResetCtrl = function($log,$scope,$state,$ionicLoading,DataSvc,SessionSvc,KvsSvc) {
  var self = this;

  $scope.profile = SessionSvc.user_profile();

  self.reset = () => {
    $ionicLoading.show();
    DataSvc.data('zombie mode','hm_dev','reset_and_populate')
      .then( r => { $ionicLoading.hide();
                    KvsSvc.reset_user().then(ok => { $state.go('tutorial'); },
                                             err => { $log.error('reset.ctrl.js ' + err);
                                                      $state.go('system_failure'); }); },
             e => { $log.error('reset.ctrl.js rejected ' + hm.dbg_object(e)); 
                    $ionicLoading.hide();
                    $state.go('tutorial'); });
  };
};
