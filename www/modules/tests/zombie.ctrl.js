/**
 * dev mode
 * Controlleur de gestion des identités
 * Les fonctionnalités du controlleur sont accéssibles depuis le menu utilisateur.
 * i.e. l'utilisateur doit être identifié au préalable
 *
 * author Renaud Tripathi <ganeshrd@laposte.net>
 */
var ZombieCtrl = function($log,$scope,$state,KvsSvc,DataSvc,SessionSvc,UsersSvc) {
  var self = this;


  /**
   * Supprime les entrées _id et pwd de l'espace de stockage et re-initialise l'application
   */
  self.reset_user = () => { KvsSvc.reset_user().then(r => { $state.go('tutorial'); }).catch(angular.noop); };

  self.login = (user) => {
    KvsSvc.store_object({'id':user.id,'pwd':user.pwd})
      .then(o => { DataSvc.send({action:'user_state'})
                     .then(r => { $log.debug('zombie.ctrl.js ' + r); 
                                  SessionSvc.set_user_state(r.value); 
                                  $scope.profile = SessionSvc.user_profile(); })
                       .then(s => { $state.go("nav.news"); }).catch(angular.noop); });
  }; 

  function init() { UsersSvc.get_users().then( resp => { $scope.users = resp.value; }).catch(angular.noop); };

  init();
};
