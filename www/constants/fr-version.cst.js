angular.module('hm_constants')
  .constant('french_version',
{ 
  "brand" : "HouseMatch",
  "btn_prev" : "Précédent",
  "btn_next" : "Suivant",
  "btn_cancel" : "Annuler",
  "btn_abort" : "Abandonner",
  "btn_agreed" : "Compris",
  "btn_rate" : "Évaluer",
  "btn_retry" : "Réessayer",
  "btn_deny" : "Refuser",
  "btn_publish" : "Publier",
  "btn_validate" : "Valider",
  "btn_search" : "Chercher",
  "btn_research" : "Rechercher",
  "btn_edit" : "Modifier",
  "btn_save" : "Enregistrer",
  "from" : "À partir du",
  "from-period" : "Du",
  "to" : "au",
  "txt_yes" : "oui",
  "txt_no" : "non",
  "rental" : "location",
  "sublease" : "sous-location",
  "shared"   : "colocation",
  "proposal"    : "Offre",
  "search"  : "Demande",
  "id_challenge/header" : "Code de vérification",
  "id_challenge/sub-header" : "Nous vous avons envoyé un code de vérification par SMS.",
  "id_challenge/input-label" : "CODE DE VÉRIFICATION",
  "id_challenge/error-msg" : "Code érroné",
  "id_challenge/no-sms" : "Vous n'avez pas reçu de SMS ?",
  "id_challenge/no-sms-link" : "Me renvoyez un SMS",
  "id_challenge/edit-phone-number" : "Modifier mon numéro",
  "form-for-tenant/title" : "Publier une offre",
  "form-for-lessor/title" : "Publier une demande",
  "form-for-tenant/renting-tip" : "Quelle offre de logement faites-vous ?",
  "form-for-lessor/renting-tip" : "Quelle demande de logement avez-vous ?",
  "adds/title" : "Mes annonces",
  "adds/tip" : "POUR PUBLIER UNE NOUVELLE ANNONCE,RENDEZ-VOUS DANS LE MENU.",
  "modals/edit/title" : "Que voulez-vous faire ?",
  "modals/confirm/delete" : "Voulez-vous supprimez votre annonce définitivement ?",
  "filter" : {
               "from" : {
                          "0" : "Dates"
                        },
               "charge" : {
                            "0" : "Loyer"
                          },
               "geo" : {
                         "0" : "Lieu, ville, code postal..."
                       },
               "period" : {
                            "0" : "indéterminée"
                          },
               "goods" : {
                           "0" : "Studio",
                           "1" : "Studio meublé",
                           "2" : "{{ room }} pièces",
                           "3" : "{{ room }} pièces meublées",
                           "4" : "Type : studio, 2 pièces..."
                         }
             },
  "directive" : {
                  "renting" : {
                                "0" : "Location",
                                "1" : "Sous",
                                "1.1" : "location",
                                "2" : "Colocation",
                                "3" : "Tous",
                                "4" : "offre",
                                "5" : "demande",
                                "6" : "OFFRE",
                                "7" : "DEMANDE",
                                "8" : "e cercle",
                                "9" : "1 logement",
                                "10" : "En recherche active",
                                "11" : "À l'écoute du marché",
                                "12" : "Quelle offre de logements faites-vous\u00A0?",
                                "13" : "Dites-en plus sur votre logement"
                              },
                  "geo" : {
                            "0" : "Adresse",
                            "1" : "Adresse :",
                            "2" : "Souhaitez-vous afficher votre adresse exacte\u00A0?"
                          },
                  "for" : {
                            "0" : "Je cherche",
                            "0.1" : "un logement",
                            "1" : "Je propose",
                            "1.1" :"un logement"
                          },
                  "goods" : {
                              "0" : "Logement",
                              "1" : "S'agit-il d'un logement meublé\u00A0?"
                            },
                  "goods_bar" : {
                                  "0" : "Nombre de pièces",
                                  "1" : "Studio"
                                },
                  "period" : {
                               "0" : "Période de disponibilité",
                               "1" : "Type de durée",
                               "2" : "semaine",
                               "3" : "mois"
                             },
                  "charge" : {
                               "0" : "Loyer",
                               "1" : "Loyer max. : "
                             },
                  "intent" : {
                               "0" : "À l'écoute",
                               "1" : "du marché",
                               "2" : "En recherche",
                               "3" : "active"
                             }
                },
  "component" : {
                  "addvert" : {
                                "0" : "Votre entreprise",
                                "1" : "Message promu",
                                "2" : "Assureur, démenageur, annonceur...",
                                "3" : "Annoncez ici\u00A0!",
                                "4" : "Pour en savoir plus, ",
                                "4.1" : "contactez-nous."
                              }
                },
  "modal" : {
              "network_failure" : {
                                    "0" : "Erreur de connexion",
                                    "1" : "Echec de la connexion au serveur. Vérifiez votre connexion et réessayez."
                                  },
               "contacts" : {
                              "0" : "<< HouseMatch >> souhaite accéder à vos contacts",
                              "1" : "HouseMatch ne revend pas vos données, n'envoie rien et ne publie rien sans votre accord.",
                              "2" : "Période de disponibilité"
                            },
               "images" : {
                            "0" : "Prendre une photo",
                            "1" : "Sélectionner dans la galerie"
                          },
               "about" : {
                           "0" : "Comment\nça marche\u00A0?",
                           "1" : "HouseMatch est un réseau social: vous êtes mis en contact uniquement avec les membres de votre réseau inscrits sur HouseMatch. Parlez de HouseMatch autour de vous pour augmenter la portée de votre réseau\u00A0!",
                           "2" : "Nous utilisons le carnet d'adresse téléphonique comme Whatsapp",
                           "3" : "Nous vous mettons en contact avec les personnes de votre réseau élargi",
                           "4" : "Nous ne diffusons pas vos contacts",
                           "5" : "Nous n'envoyons rien sans votre accord",
                           "6" : "Et c'est gratuit\u00A0!"
                         }
            },
  "init" : {
             "0" : "Bienvenue sur HouseMatch",
             "1" : "Vous cherchez\nun locataire, un colocataire, un sous-locataire de confiance\u00A0?",
             "2" : "Location,\ncolocation,\nsous-location...",
             "3" : "Vous aimeriez trouver facilement un logement dans votre réseau\u00A0?",
             "4" : "HouseMatch,\nle réseau social du logement",
             "5" : "Trouvez logement et personnes de confiance via vos contacts téléphoniques.",
             "6" : "Ne cherchez plus seul : faites jouer votre réseau\u00A0!",
             "7" : "Avec HouseMatch, les intermédiaires sont récompensés\u00A0!",
             "8" : "Nous n'envoyons pas de messages à vos contact, nous protégeons vos données.",
             "9" : "Pour en savoir plus",
             "10" : "Vous",
             "11" : "Mes contacts",
             "12" : "Personne de confiance",
             "13" : "Logement",
             "14" : "1er\ncercle",
             "15" : "2ème\ncercle",
             "16" : "3ème\ncercle",
             "17" : "Demande",
             "18" : "Offre"
           },
  "policy" : { 
               "privacy" : {
                             "0" : "Politique de condidentialié"
                           },
               "usage" : {
                           "0" : "Conditions générales d'utilisation"
                         }
             },
  "register" : {
                 "tel" : {
                           "0" : "Quel est votre numéro\u00A0?",
                           "1" : "Nous ne communiquons pas votre numéro de téléphone sans votre accord",
                           "2" : "Ex. 0646323108",
                           "3" : "NUMERO DE PORTABLE",
                           "4.1" : "Merci d’indiquer un numéro de portable valide",
                           "4.21" : "Un autre utilisateur est déjà inscrit avec ce numéro...",
                           "4.22" : "Serait-ce",
                           "4.23" : "vous\u00A0?",
                           "5.1" : "En vous inscrivant, vous acceptez les ",
                           "5.2" : "Conditions générales d'utilisation",
                           "5.3" : " et la ",
                           "5.4" : "Politique de confidentialité",
                           "5.5" : " de HouseMatch"
                         },
                "id_challenge" : {
                                   "failure" : "Erreur de saisie"
                                 },
                "name" : {
                           "0" : "Et votre nom\u00A0?",
                           "1" : "Voici comment cela apparaitra dans votre réseau.",
                           "2" : "PRÉNOM",
                           "3" : "NOM",
                           "4" : "Chaîne de caractère non conforme.",
                           "5" : "Merci d’indiquer un nom et un prénom avec au moins 2 lettres"
                         },
                "add" : {
                          "0" : "Qu'est-ce qui\nvous amène ici\u00A0?",
                          "1" : "Vous pourrez modifier ou ajouter des recherches par la suite.",
                          "2" : "Dernières informations..."
                        }
              },
  "register-number" : {
                    "form" : {
                          "0" : "Adresse",
                          "1" : "Ville",
                          "2" : "Code postal",
                          "3" : "Merci pour vos réponses & bonne continuation sur HouseMatch\u00A0!"
                        },
                    "submit" : {
                          "0" : "Numéro d’enregistrement en ligne",
                          "1" : "Merci de nous fournir votre numéro d’enregistrement pour respecter les obligations légales\u00A0:",
                          "2" : "Vous n’avez pas de numéro d’enregistrement en ligne\u00A0?",
                          "3" : "De quoi s’agit-il\u00A0?"
                    }
  },              
   "menu" : {
              "0" : "Menu",
              "1" : "Mon compte",
              "2" : "Fil d'actualité",
              "3" : "Messagerie",
              "4" : "Matchs",
              "5" : "Modifier/Supprimer",
              "5.1" : "mes annonces",
              "6" : "Publier une offre",
              "7" : "Publier une demande",
              "8" : "Propositions",
              "9" : "Vos gains",
              "10" : "Blacklist",
              "10.1" : "bloquer certains contacts de mon réseau",
              "11" : "Paramètres",
              "12" : "FAQ",
              "13" : "English",
              "14" : "Contactez-nous"
            },
   "news" : {
              "0" : "Fil d'actualité",
              "1" : "Pas d'actualité pour le moment...",
              "2" : "Mais ne perdez pas espoir\u00A0!",
              "3" : "Parlez d'Housematch autour de vous.",
              "4" : "1 personne du réseau fait parfois toute la différence\u00A0!"
            },
   "exit" : {
              "user_failure" : {
                                 "0" : "Désolé...",
                                 "1" : "HouseMatch ne peut pas fonctionner sans l'accès à vos contacts.",
                                 "2" : "HouseMatch ne revend pas vos données, n'envoie rien et ne publie rien sans votre accord.",
                                 "3" : "Bonnes recherches\u00A0!",
                                 "4" : "J'ai changé d'avis"
                               },
              "system_failure" : {
                                 "0" : "Oups...",
                                 "1" : "Il semble qu'il y ait un problème...",
                                 "2" : "Merci de relancer l'application.",
                                 "3" : "Si le problème persiste, merci de nous envoyer un message ici."
                               }
            },
  "adds" : {
             "0" : "Édition de votre offre",
             "1" : "Édition de votre demande"
           },
  "profile" : {
                "0" : "Mon compte",
                "1" : "Modifier",
                "2" : "{{ age }} ans",
                "3" : "Un profile complet",
                "4" : "=",
                "5" : "3 x plus de chance de trouver",
                "6" : "À propos de moi",
                "7" : "Mail",
                "8" : "Âge",
                "9" : "Activité"
              },
  "description" : {
                    "0" : "Titre de l'annonce\u00A0: (Max 50 caractères)",
                    "1" : "Description du logement : <span>(Taille, emplacement, équipement...)</span>"
                  },
  "success" : {
                    "0" : "Félicitations\u00A0!",
                    "1" : "Votre annonce a été publiée dans votre réseau.",
                    "2" : "Consulter mon annonce",
                    "3" : "Voir mes matchs"
                  },
  "share" : {
                    "0" : "Nous avons besoin de votre aide pour faire connaitre HouseMatch…",
                    "1" : "Nous aider, c’est vous aider\u00A0!",
                    "2" : "(avec plus de possibilités de matchs)",
                    "3" : "Vous connaissez quelqu’un qui cherche ou propose un logement\u00A0?",
                    "5" : "Publier/Envoyer le message suivant\u00A0?",
                    "9" : "Envoyer par SMS",
                    "10" : "Publier sur facebook"
                  },
  "rate" : {
                    "0" : "Laissez une note/un commentaire sur HouseMatch…"
                  }                                     
 });
