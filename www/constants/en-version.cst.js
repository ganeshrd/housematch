angular.module('hm_constants')
  .constant('english_version',
{
  "brand" : "HouseMatch",
  "btn_prev" : "Previous",
  "btn_next" : "Next",
  "btn_cancel" : "Cancel",
  "btn_abort" : "Abort",
  "btn_agreed" : "Ok",
  "btn_rate" : "Rate",
  "btn_retry" : "Retry",
  "btn_deny" : "Deny",
  "btn_publish" : "Publish",
  "btn_validate" : "Validate",
  "btn_edit" : "Edit",
  "btn_save" : "Save",
  "from" : "From",
  "from-period" : "From",
  "to" : "to",
  "txt_yes" : "yes",
  "txt_no" : "no",
  "rental" : "location",
  "sublease" : "sous-location",
  "shared"   : "colocation",
  "proposal"    : "Offer",
  "search"  : "search",
  "id_challenge/header" : "Authentication code",
  "id_challenge/sub-header" : "We sent you an authentication code.",
  "id_challenge/input-label" : "AUTHENTICATION CODE",
  "id_challenge/error-msg" : "wrong authentication code",
  "id_challenge/no-sms" : "You did not received a SMS?",
  "id_challenge/no-sms-link" : "Send me another one",
  "id_challenge/edit-phone-number" : "Edit my phone number",
  "form-for-tenant/title" : "Create a proposal",
  "form-for-lessor/title" : "Publish your search",
  "form-for-tenant/renting-tip" : "Define your rental types",
  "form-for-lessor/renting-tip" : "Define your rental types",
  "adds/title" : "My adds",
  "adds/tip" : "CREATE A NEW ADD FROM THE MENU.",
  "modals/edit/title" : "Choose an option.",
  "modals/confirm/delete" : "Do you want to remove your add permanently?",
  "filter" : {
               "from" : {
                          "0" : "Dates"
                        },
               "charge" : {
                            "0" : "Rent"
                          },
               "geo" : {
                         "0" : "place, zip code..."
                       },
               "period" : {
                            "0" : "N.D."
                          },
               "goods" : {
                           "0" : "Single room",
                           "1" : "Single room with furnitures",
                           "2" : "{{ room }} rooms",
                           "3" : "{{ room }} rooms with furnitures",
                           "4" : "Estate type : single, 2 rooms..."
                         }
             },
  "directive" : {
                  "renting" : {
                                "0" : "Rental",
                                "1" : "Sub",
                                "1.1" : "lease",
                                "2" : "Shared",
                                "3" : "All",
                                "4" : "propose an estate",
                                "5" : "search a place",
                                "6" : "PROPOSE",
                                "7" : "SEEK",
                                "8" : "nth realm"
                              },
                  "geo" : {
                            "0" : "Address",
                            "1" : "Address :",
                            "2" : "Do you want to display the full address?"
                          },
                  "for" : {
                            "0" : "In search",
                            "0.1" : "of a place",
                            "1" : "I am proposing",
                            "1.1" :"a place"
                          },
                  "goods" : {
                              "0" : "Estate",
                              "1" : "Is there any furnitures?"
                            },
                  "goods_bar" : {
                                  "0" : "Estate type",
                                  "1" : "Single room"
                                },
                  "period" : {
                               "0" : "Period of availability",
                               "1" : "Period type",
                               "2" : "week",
                               "3" : "month"
                             },
                  "charge" : {
                               "0" : "Rent",
                               "1" : "Max rent : "
                             },
                  "intent" : {
                               "0" : "Open",
                               "1" : "to proposals",
                               "2" : "Active",
                               "3" : "seeking"
                             }
                },
  "component" : {
                  "addvert" : {
                                "0" : "Your organisation",
                                "1" : "Message promoted",
                                "2" : "insurer, mover, announcer ...",
                                "3" : "place your add here!",
                                "4" : "Send us a ",
                                "4.1" : "message."
                              }
                },
  "modal" : {
              "network_failure" : {
                                    "0" : "Network failure",
                                    "1" : "Network failure while trying to reach the server. Please check on your network settings and retry."
                                  },
               "contacts" : {
                              "0" : "<< HouseMatch >> needs to access your contacts.",
                              "1" : "HouseMatch will hold your data private as long as you say so.",
                              "2" : "Availability period"
                           },
               "images" : {
                            "0" : "Take a picture",
                            "1" : "Select from galery"
                          },
               "about" : {
                           "0" : "How\ndoes it work?",
                           "1" : "HouseMatch is a social network. You'll be contacted only with other users from your personal network. Increase your network leverage by talking about us.",
                           "2" : "We use your cellphonebook just like Whatsapp",
                           "3" : "We connect you with your relational network",
                           "4" : "We do not expose your contacts",
                           "5" : "We do nothing without your agreement",
                           "6" : "For free!"
                         }
            },
  "init" : {
             "0" : "Welcome to HouseMatch",
             "1" : "Looking for a trusted tenant?",
             "2" : "rental, shared, sublease...",
             "3" : "You're in search of a quick match?",
             "4" : "HouseMatch, the housing social network",
             "5" : "Housing and trusted tenant through your phonebook",
             "6" : "Leverage your personal network!",
             "7" : "You can also earn money as an intermediary",
             "8" : "We do not send messages to your contacts, we care about your privacy",
             "9" : "more..."
           },
  "policy" : {
               "privacy" : {
                             "0" : "Privacy policy"
                           },
               "usage" : {
                           "0" : "General term conditions"
                         }
            },
  "register" : {
                 "tel" : {
                           "0" : "What is your cell phone number?",
                           "1" : "Your phone number stays private until you choose not.",
                           "2" : "e.g. 0646323108",
                           "3" : "CELL PHONE NUMBER",
                           "4.1" : "Please provide a valide cell phone number",
                           "4.21" : "Someone else is using this number ... ",
                           "4.22" : "Is that",
                           "4.23" : "you?",
                           "5.1" : "When subscribing you approve ",
                           "5.2" : "the genral condition of use",
                           "5.3" : " and ",
                           "5.4" : "the houseMatch's privacy policy",
                           "5.5" : ""
                         },
                "id_challenge" : {
                                   "failure" : "Code mismatch"
                                 },
                "name" : {
                           "0" : "What is your name?",
                           "1" : "Choose the name that will be displayed through your personal network.",
                           "2" : "SURNAME",
                           "3" : "NAME",
                           "4" : "Unauthorized string."
                         },
                "add" : {
                          "0" : "What brings you here?",
                          "1" : "You can always add or edit your submissions later on."
                        }
               },
   "menu" : {
              "0" : "Menu",
              "1" : "Account",
              "2" : "Personal network",
              "3" : "Mails",
              "4" : "Matchs",
              "5" : "Edit/Delete",
              "5.1" : "my adds",
              "6" : "Propose",
              "7" : "Search",
              "8" : "Proposals",
              "9" : "Your share",
              "10" : "Blacklist",
              "10.1" : "disable some of your contacts",
              "11" : "Parameters",
              "12" : "FAQ",
              "13" : "French",
              "14" : "Contact us"
            },
   "news" : {
              "0" : "News",
              "1" : "No news yet...",
              "2" : "Do not despair!",
              "3" : "Talk about us around.",
              "4" : "A single person in your network can make the difference!"
            },
   "exit" : {
              "user_failure" : {
                                 "0" : "Sorry...",
                                 "1" : "HouseMatch really needs your contacts in order to operate.",
                                 "2" : "HouseMatch keeps your data private.",
                                 "3" : "See you !",
                                 "4" : "I changed my mind"
                               },
              "system_failure" : {
                                 "0" : "Oops ...",
                                 "1" : "We have a problem...",
                                 "2" : "Try to restart the application.",
                                 "3" : "If the problem isn't fixed, leave us a message here."
                               }
            },
  "adds" : {
             "0" : "Edit your proposal",
             "1" : "Edit your search request"
           },
  "profile" : {
                "0" : "My account",
                "1" : "Edit",
                "2" : "{{ age }} years old",
                "3" : "A completed profile",
                "4" : "increase",
                "5" : "your chance of success"
              }
});
