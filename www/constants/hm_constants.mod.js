angular.module('hm_constants',[])

  .constant('SUPPORTED_LANGUAGES',[ 'fr','en' ])
  .constant('LOCALE_REGEX', /^([a-zA-Z]{2}).*$/i)
  .constant('country_codes',[{"name":"France métropolitaine","dial_code":"+33","code":"FR"},
                             {"name":"Guadeloupe","dial_code":"+590","code":"FR"},
                             {"name":"Guyane","dial_code":"+594","code":"FR"},
                             {"name":"La reunion","dial_code":"+262","code":"FR"},
                             {"name":"Martinique","dial_code":"+596","code":"FR"},
                             {"name":"Belgium","dial_code":"+32","code":"BE"},
			                       {"name":"Germany","dial_code":"+49","code":"DE"},
			                       {"name":"Canada","dial_code":"+1","code":"CA"}])
  .constant('cell_phones_re', [ /^\+33(6|7)\d{8}$/,
			        /^\+590(6|7)\d{8}$/,
			        /^\+596(6|7)\d{8}$/,
			        /^\+594(6|7)\d{8}$/,
			        /^\+262(6|7)\d{8}$/,
			        /^\+32(46|47|48|49)\d{6}$/,
			        /^\+49(15|16|17)\d{9}$/,
			        /^\+1(204|236|250|289|306|403|416|418|438|)\d{7}$/,
			        /^\+1(450|506|514|519|579|581|604|613|647|)\d{7}$/,
			        /^\+1(705|709|778|780|807|819|867|902|905)\d{7}$/ ])
  .constant('DATABASE','hm.db')
  .constant('WS_TIMEOUT',500) // interval entre les cycles 'hm_digest'.
  .constant('FTP_TIMEOUT',3000) // interval entre les cycles 'hm_digest'.
  .constant('MAX_ATTEMPT',14) // nombre max de cycle 'hm_digest'.
  .constant('DB_SIZE',1000000)
  .constant('TRANSPORT','https')
  .constant('RENTING', { 'rental' : 2, 'sublease' : 3, 'shared' : 5 })
  .constant('STATE_BACK_DISABLE', ["welcome2","welcome3", "welcome4", "welcomeFinal", "register.name"])
  .constant('FIREBASE_PARAMS', {
                                  // apiKey: "AIzaSyB1HN7AJHoLrB2aXuMdav2by9esDFrS6U4",
	                               //  authDomain: "housematch-177515.firebaseapp.com",
	                               //  databaseURL: "https://housematch-177515.firebaseio.com",
	                               //  projectId: "housematch-177515",
	                               //  storageBucket: "gs://housematch-177515.appspot.com/",
	                               //  messagingSenderId: "638157938787"
                                  apiKey: "AIzaSyA4zQPPm9NfIKOPx6PVE4A6NkX6SNsWIz4",
                                  authDomain: "housemutch.firebaseapp.com",
                                  databaseURL: "https://housemutch.firebaseio.com",
                                  projectId: "housemutch",
                                  storageBucket: "housemutch.appspot.com",
                                  messagingSenderId: "942816337799"
		                           });
