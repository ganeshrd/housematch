angular.module('hm_directives')
/**
 * Directive google-autocomplete.
 *
 * https://github.com/jvandemo/angularjs-google-maps
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('googleplace',['$log', function($log) {

  let controller = ['$scope',function($scope) {
    let self = this;
  }];

  let link = function(scope,element,attr,model) {
    let options = {
      types : ['geocode']
      // componentRestrictions : [] // future : a user could be able to restrict his search
    };
    scope.gPlace = new google.maps.places.Autocomplete(element[0],options);
    google.maps.event.addListener(scope.gPlace,'place_changed',function() {
      console.log("fired");
      scope.$apply(function() { model.$setViewValue(scope.gPlace.getPlace()); });
    });
  };
  return {
           require: 'ngModel', // make ngModel be the fourth link function argument
           link : link,
           controller : controller,
           controllerAs : 'place_ctrl'
         };
}]);
