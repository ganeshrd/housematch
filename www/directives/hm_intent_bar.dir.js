angular.module('hm_directives')
/**
 * directive pour la gestion de la bar du choix de visibilité d'une annonce de HouseMatch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */

.directive('hmIntentBar',['$log','$translate',function($log,$translate) {

  let controller = ['$scope',function($scope) {
    let self = this;
    self.button_state = {activ:false,open:false};
    self.$onInit = function() { if(['activ','open'].indexOf($scope.intent) >= 0) self.button_state[$scope.intent] = true; };
    $scope.is_selected = (val) => { return self.button_state[val]; };

    /**
     * update the state object i.e. self.button_state when a button
     * is clicked. Only one button can be triggered at a time.
     * @param value Integer the index of the property to be set to true.
     * All other properties are set to false.
     */
     $scope.toggle_button = (value) => {
       angular.forEach(self.button_state,function(v,k) { self.button_state[k] = false; });
       self.button_state[value] = true;
       $scope.intent = value;
     };
  }];

  return {
           restrict : 'E',
           controller : controller,
           controllerAs : 'intent_ctrl',
           templateUrl : 'templates/directives/intent_bar.html',
           scope : {
                  intent : '=ngModel'
           }
         }
}]);
