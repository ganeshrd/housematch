angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente une annonce telle qu'exposée dans le fil d'actualité
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmNewsItem',['$log',function($log) {

  let controller = ['$log','$scope',function($log,$scope) { 
  }];

  return {
           restrict : 'E',
           templateUrl : 'templates/directives/news.html',
           scope : {
                     add : '=ngModel',
                     index : '@'
                   }, 
           controller : controller,
           controllerAs : 'news_ctrl',
         };
}]);
