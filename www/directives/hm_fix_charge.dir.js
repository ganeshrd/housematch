angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente le bouton de sélection du type de logement.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmFixCharge',['$log',function($log) {

  let controller = ['$log','$scope',function($log,$scope) { 
    let self = this;
    self.charge;
    self.assign_model = () => { $scope.charge = self.charge; };
    self.$onInit = function() { self.charge = $scope.charge; };
                                
  }];

  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/fix_charge.html',
           scope : {
                     charge : '=ngModel',
                   }, 
           controller : controller,
           controllerAs : 'fix_charge_ctrl',
         };
}]);
