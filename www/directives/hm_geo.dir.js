angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente le bouton de saisie de l'adresse.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmGeo',['$log','$ionicModal','$translate', 'LocalisationSvc', function($log, $ionicModal, $translate, LocalisationSvc) {

  let controller = ['$scope',function($scope) {
    let self = this;
    self.title = $translate.instant('directive.geo.0');
    // self.place = undefined;
    // self.displayed_address = undefined;
    self.$onInit = function() { if(angular.isDefined($scope.geo)) self.geo = $scope.geo; };

    /**
     * assign main controller scope
     */
    $scope.save = (location) => {
      setTimeout(()=> {
        if(angular.isDefined(location)) {
          let coord = location.geometry['location'];
          let lat = coord.lat().toString(), lng = coord.lng().toString();
          let add = location.formatted_address;
          self.geo = { coordinates: {'longitude':lng,'latitude':lat}, address : add };
          $scope.geo = self.geo;
        }
      },10);

    };
       LocalisationSvc.getPosition().then(country => {})
  }];


  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/geo.html',
           scope : {
                    geo : '=ngModel'
                   },
           controller : controller,
           controllerAs : 'geo_ctrl'
         };
}]);
