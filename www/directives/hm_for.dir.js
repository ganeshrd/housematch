angular.module('hm_directives')
/**
 * Présente un switch à l'utilisateur qui définit
 * La cible de sa recherche.
 * i.e. annonce pour des locataires (tenant) ou propriétaires (lessor)
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.directive('hmFor',['$log',function($log) {

  return {
           restrict : 'E',
           templateUrl : 'templates/directives/for.html',
           scope : {
                     for : '=ngModel',
                   }, 
         };
}]);


