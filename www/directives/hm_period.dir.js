angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente le bouton de sélection de la période de location.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmPeriod',['$log','$ionicModal',function($log,$ionicModal) {

  var controller = ['$scope','$morphCarousel','$translate',function($scope,$morphCarousel,$translate) {
    let self = this;
    /**
     * utility function
     * fill items array
     * @param max is max items count
     * @return Array
     */
     self.set_durations = (max) => {
       let a = [];
       for(let i=1;i<=max;i++) { a.push({ duration : i }); }
       return a;
     };

    self.title = $translate.instant('directive.period.0');
    self.type_from_ctrl = 'week';
    self.current_items = 'week';

    $scope.modal = null;
    $scope.week_items = self.set_durations(7);
    $scope.month_items = self.set_durations(36);
    $scope.data = { item : $scope.week_items[2] };
    
    /**
     * display the modal
     */
    $scope.modal_on = () => { $scope.modal.show(); 
                              $morphCarousel.update('week-carousel'); 
                              $morphCarousel.update('month-carousel'); 
    };

    self.save = () => {
      $scope.period = { 
                        period_type : self.type_from_ctrl,
                        duration : $scope.data.item.duration 
                      };
      $scope.modal.hide();
    };

    /**
     * triggers the modal
     */
    $ionicModal.fromTemplateUrl('templates/modals/period.html', { 
                                                                  animation : 'slide-in-down',
                                                                  scope : $scope  
                                                                })
        .then( modal => { $scope.modal = modal; });
  }];

  let link = function(scope,element,attr,ctrls) {
    /**
     * The click event triggers the modal
     */
    element.bind('click',function() { scope.modal_on(); });
    ctrls.ngModel.$validators.isSet = () => { 
                                              if(ctrls.form.$submitted) 
                                              { 
                                                // $log.debug('hm_period.dir.js if'); 
                                                return angular.isDefined(scope.period) && 
                                                angular.isDefined(scope.period.period_type) && 
                                                angular.isDefined(scope.period.duration); 
                                              }
                                              else 
                                              {
                                                // $log.debug('hm_period.dir.js else');
                                                return true;
                                              } };
  }; 

  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/period.html',
           scope : {
                     period : '=ngModel'
                   }, 
           link : link,
           controller : controller,
           controllerAs : 'period_ctrl'
         };
}]);
