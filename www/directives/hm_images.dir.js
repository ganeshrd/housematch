angular.module('hm_directives')
/**
 * Directive Housematch.
 * Sélection des images de l'annonce.
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.directive('hmImages',['$log',function($log) {

  let controller = ['$scope','$window','ImgSvc', 'ModalsSvc', 'FirebaseSvc', function($scope,$window,ImgSvc, ModalsSvc, FirebaseSvc) {
    let self = this;
    $scope.snapshot = () => { $log.debug('profile_editor.ctrl.js TODO snapshot');
                              ImgSvc.do_camera('DATA_URL', 'CAMERA').then((url) => { self.addImage(url); });
                              self.modal.hide();
                              self.modal.remove(); };

    $scope.galery = () => { $log.debug('profile_editor.ctrl.js TODO galery');
                            self.modal.hide();
                            ImgSvc.do_camera('DATA_URL', 'PHOTOLIBRARY').then((url) => { self.addImage(url); });
                            self.modal.remove(); };

    $scope.cancel = () => { self.modal.hide();
                            $ionicLoading.hide();
                            self.modal.remove(); };

    // self.images = []; // why ?

    $scope.params = { img_container_length : $scope.images.length };
    $scope.add_picture = () => {
      ModalsSvc.chooseMedia($scope).then((modal) => {
          self.modal = modal;
        });
    };

    self.deletePitures = (index) => {
      $scope.images.splice(index, 1);
      $scope.params.img_container_length = $scope.images.length;
    }
    self.addImage = (src) => {
      let img = {'src':src, isBase64: true};
      img.idx = $scope.images.length;
      $scope.images.push(img);
      $scope.params.img_container_length = $scope.images.length;
    }
  }];


  let link = function(scope,element,attr,ctrls) {

    ctrls.ngModel.$validators.isSet = function() { if(ctrls.form.$submitted) return Array.isArray(scope.images) && scope.images.length > 0;
                                                   else return true; };
  };

  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/images.html',
           scope : {
                     images : '=ngModel',
                   },
           link : link,
           controller : controller,
           controllerAs : 'images_ctrl'
         };
}]);
