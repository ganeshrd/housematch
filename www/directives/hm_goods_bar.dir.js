angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente une barre de sélection du type de logement.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmGoodsBar',['$log','$translate',function($log,$translate) {

  let controller = ['$scope',function($scope) {
    let self = this;
    self.title = $translate.instant('directive.goods_bar.0');
    self.subtitle = '';
    self.button_state = {0:false,1:false,2:false,3:false,4:false};
    self.$onInit = function() { if(angular.isDefined($scope.goods) && Number.isInteger($scope.goods)) self.button_state[$scope.goods] = true; };
    $scope.is_selected = (val) => { return self.button_state[val]; };

    /**
     * update the state object i.e. self.button_state when a button
     * is clicked. Only one button can be triggered at a time.
     * @param value Integer the index of the property to be set to true.
     * All other properties are set to false.
     */
     $scope.toggle_button = (value) => {
       console.log('toggle_button value', value);
       angular.forEach(self.button_state,function(v,k) { self.button_state[k] = false; });
       self.button_state[value] = true;
       $scope.goods = value;
     };
  }];

  return {
           restrict : 'E',
           controller : controller,
           controllerAs : 'goods_bar_ctrl',
           templateUrl : 'templates/directives/goods_bar.html',
           scope : {
                     goods : '=ngModel'
                   }
         }
}]);
