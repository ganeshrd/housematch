angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente le bouton de sélection du type de logement.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmRangeCharge',['$log', function($log) {

  let controller = ['$log', '$scope', function($log, $scope) {
    let self = this;
    $scope.charge = $scope.charge || 0;
    const formatter = new Intl.NumberFormat('fr-FR', {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 0
    })


    $scope.getCharge = () => {
      return formatter.format($scope.charge)
    }
    $scope.getRangeStyle = (val) => {
      return {
        'background-image' :
                    '-webkit-gradient(linear, left top, right top, '
                    + 'color-stop(' + val/3000 + ', #30A75D), '
                    + 'color-stop(' + val/3000 + ', #d3d3db)'
                    + ')'
      }
    }
  }];


  // var val = $scope.range / 5000;


  let link = function(scope,element,attrs,ctrls) {
    ctrls.ngModel.$validators.isSet = function() { if(ctrls.form.$submitted) { let v = ctrls.form.charge.$viewValue;
                                                                               let p = v != null && !isNaN(v) && (v == parseInt(v,10));
                                                                               return ctrls.form.charge.$valid && p; }
                                                   else return true; }

  };

  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/range_charge.html',
           scope : {
                     charge : '=ngModel',
                   },
           controller : controller,
           controllerAs : 'range_charge_ctrl',
           link : link
         };
}]);
