angular.module('hm_directives')
/**
 * Présente un switch à l'utilisateur qui définit
 * son positionnement par rapport à son réseau
 * i.e. en recherche active ou à l'écoute du marché
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.directive('hmUserIntent',['$log',function($log) {

  return {
           restrict : 'E',
           templateUrl : 'templates/directives/user_intent.html',
           scope : {
                     intent : '=',
                   }, 
         };
}]);


