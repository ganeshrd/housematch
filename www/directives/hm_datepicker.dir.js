angular.module('hm_directives')
/**
 * directive pour la gestion des datepicker de HouseMatch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.directive('hmDatepicker',['$log','$ionicModal',function($log,$ionicModal) {
  var controller = ['$scope',function($scope) {
    let self = this;
    self.modal = null;
    self.date_from_calendar = $scope.date; // undefined
    /**
     * switch on modal
     */
    $scope.modal_on = () => {
      $ionicModal.fromTemplateUrl('templates/modals/dateModal.html', { animation : 'slide-in-down',
                                                                       scope : $scope  
                                                                     })
        .then( modal => { self.modal = modal; 
                          self.modal.show(); });
    };
    self.date_reset = () => { 
      $scope.date = undefined; 
      self.date_from_calendar = $scope.date;
    };
    self.cancel = () => {
      // self.date_reset(); why reset on a cancel ?
      self.modal.hide();
      self.modal.remove();
      // $log.debug('hm_datepicker.dir.js date : ' + $scope.date);
      // $log.debug('hm_datepicker.dir.js date from calendar: ' + self.date_from_calendar);
    };
    /**
     * what for ??
     * useless in the UI and create a bug.
     */
    self.reset = () => {
      self.date_reset();
      self.modal.hide();
      // $log.debug('hm_datepicker.dir.js date : ' + $scope.date);
      // $log.debug('hm_datepicker.dir.js date from calendar: ' + self.date_from_calendar);
    };
    self.save = () => {
      // let d = new Date($scope.date).getTime();
      let d = new Date(self.date_from_calendar).getTime();
      $scope.date = d;
      self.modal.hide();
      self.modal.remove();
      // $log.debug('hm_datepicker.dir.js date : ' + $scope.date);
      // $log.debug('hm_datepicker.dir.js date from calendar: ' + self.date_from_calendar);
    };
    self.convertDate = (inputFormat) => {
      function pad(s) { return (s < 10) ? '0' + s : s; }
      let d = new Date(inputFormat);
      return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
    };
    }];
  function link(scope,element,attr,ctrls) {
    /**
     * bind event to switch on
     */
    element.bind('click',function() { scope.modal_on(); });

    ctrls.ngModel.$validators.isSet = () => { if(ctrls.form.$submitted) return scope.date != null && !isNaN(scope.date); 
                                              else return true; };
  };

  return {
    restrict : 'E',
    require : {ngModel:'ngModel',form:'^form'},
    templateUrl : 'templates/directives/datepicker.html',
    scope : {
              date : '=ngModel' 
            },
    link : link,
    controller : controller,
    controllerAs : 'datepicker_ctrl'
 
  };
}]);

