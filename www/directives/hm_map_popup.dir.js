angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente une annonce telle qu'exposée dans le fil d'actualité
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmMapPopup',['$log',function($log) {

  let controller = ['$log','$scope',function($log,$scope) { 
  }];

  return {
           restrict : 'E',
           templateUrl : 'templates/directives/map_popup.html',
           scope : {
                     add : '=ngModel',
                     index : '@'
                   }, 
           controller : controller,
           controllerAs : 'news_ctrl',
         };
}]);
