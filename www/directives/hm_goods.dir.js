angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente le bouton de sélection du type de logement.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmGoods',['$log','$ionicModal','$translate',function($log,$ionicModal,$translate) {

  var controller = ['$scope',function($scope) {
    let self = this;
    self.goods_from_ctrl = null;
    self.furnitures_from_ctrl = false;
    self.modal = null;
    self.title = $translate.instant('directive.goods.0');
    /**
     * display the modal
     */
    $scope.modal_on = () => { self.modal.show(); };

    self.save = () => {
      $scope.goods = { goods_type : self.goods_from_ctrl,
                       furnitures : self.furnitures_from_ctrl };
      self.modal.hide();
    };
    /**
     * triggers the modal
     */
    $ionicModal.fromTemplateUrl('templates/modals/goods.html', { 
                                                                 animation : 'slide-in-down',
                                                                 scope : $scope  
                                                               })
        .then( modal => { self.modal = modal; });

  }];

  let link = function(scope,element,attr,ctrls) {
    /**
     * The click event triggers the modal
     */
    element.bind('click',function() { scope.modal_on(); });
    
    ctrls.ngModel.$validators.isSet = function() { if(ctrls.form.$submitted) 
                                                   { //$log.debug('hm_goods.dir.js if'); 
                                                     return angular.isDefined(scope.goods) && 
                                                            angular.isDefined(scope.goods.furnitures) &&
                                                            angular.isDefined(scope.goods.goods_type) &&
                                                            Number.isInteger(scope.goods.goods_type); 
                                                   }
                                                   else 
                                                   {
                                                     // $log.debug('hm_goods.dir.js else');
                                                     return true; 
                                                   } };

  }; 
  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/goods.html',
           scope : {
                     goods : '=ngModel',
                   }, 
           link : link,
           controller : controller,
           controllerAs : 'goods_ctrl'
         };
}]);
