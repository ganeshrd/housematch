angular.module('hm_directives')
/**
 * directive pour la gestion des datepicker de HouseMatch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.directive('hmRentingBar',['$log',function($log) {
  let controller = ['$scope',function($scope) {
    let self = this;
    self.types = {};

    /**
     * compute renting value out of self.types properties
     */
    self.compute_renting = () => {
      let pi = 1;
      angular.forEach(self.types,(v,k) => { pi *= v? k:1; });
      $scope.renting = pi;
    };

    /**
     * init
     * Check intial value for $scope.renting and compute self.types properties.
     */
    try
    {
      if(angular.isNumber($scope.renting))
      {
        angular.forEach([2,3,5],(k) => { self.types[k] = $scope.renting % k? false:true; });
      }
      else
      {
        angular.forEach([2,3,5],(k) => { self.types[k] = false; });
        self.compute_renting();
      }
    }
    catch (e)  // can it be reach ? ($scope).form.renting is directive argument
    {
      // $log.info('hm_client_form.dir.js ' + e.message);
      angular.forEach([2,3,5],(k) => { self.types[k] = false; });
      self.compute_renting();
    } 

    /**
     * set self.types properties to either true or false.
     * @param val boolean The value to be assigned.
     */
    self.set_types = (val) => { angular.forEach(self.types,(v,k) => { self.types[k] = val; }); };

    /**
     * user input i.e. value = 2 | 3 | 5
     */
    self.selectType = (value) => {
      if(!self.readOnly)
      {
        if(self.isAllSelected())  self.set_types(false); 
        self.types[value] = !self.types[value];
        self.compute_renting(); // sync
      }
    };

    /**
     * Toggle all flags at once if
     * readOnly is false otherwise does nothing
     */
    self.selectAllType = () => {
      if(!self.readOnly) 
      {
        if( self.isAllSelected() ) { 
          self.set_types(false);
        } else { 
          self.set_types(true);
        }
        self.compute_renting(); // sync
      }
    };

    self.isAllSelected = () => { return _.every(self.types); };
  }];

  let link = function(scope,element,attrs,ctrls) {
    ctrls.ngModel.$validators.isSet = () => { if(ctrls.form.$submitted) 
                                              {
                                                // $log.debug('hm_renting_bar.dir.js if');
                                                return angular.isDefined(scope.renting) && scope.renting != '1'; 
                                              }
                                              else 
                                              {
                                                // $log.debug('hm_renting_bar.dir.js else');
                                                return true; 
                                              } };
  };

  return {
    restrict : 'E',
    require : {ngModel:'ngModel',form:'^form'},
    templateUrl : 'templates/directives/renting_bar.html',
    scope : {
              renting : '=ngModel'
            },
    link : link,
    controller : controller,
    controllerAs : 'renting_ctrl'
  }; 

}]);
