angular.module('hm_directives')
/**
 * directive pour la gestion des datepicker de HouseMatch
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
.directive('hmStaticRentingBar',['$log',function($log) {

  return {
    restrict : 'E',
    templateUrl : 'templates/directives/static_renting_bar.html',
    scope : {
              renting : '=ngModel'
            }
  }; 

}]);
