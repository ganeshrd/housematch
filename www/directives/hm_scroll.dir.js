angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente une barre de sélection du type de logement.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmScroll',['$log','$translate', '$location', '$anchorScroll', function($log, $translate, $location, $anchorScroll) {

  let controller = ['$scope',function($scope) {
    let self = this;


     $scope.gotoBottom = $scope.cb

  }];

  return {
           restrict : 'E',
           controller : controller,
           controllerAs : 'goods_bar_ctrl',
           templateUrl : 'templates/directives/scroll.html',
           scope : {
                     cb : '&'
                   }
         }
}]);
