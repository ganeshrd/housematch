angular.module('hm_directives')
/**
 * Directive Housematch.
 * Présente le bouton de sélection du type de logement.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
.directive('hmDescription',['$log',function($log) {

  let controller = ['$scope',function($scope) { 
    let self = this;
  }];

  /**
  let link = function(scope,element,attrs,ctrls) {
    ctrls.ngModel.$validators.isSet = function() { if(ctrls.form.$submitted) { let _title = ctrls.form.title.$viewValue;
                                                                               let _text = ctrls.form.text.$viewValue;
                                                                               let _ti = angular.isDefined(_title)? _title.length > 0 : false;
                                                                               let _te = angular.isDefined(_text)? _text.length > 0 : false;
                                                                               return ctrls.form.title.$valid && ctrls.form.text.$valid &&
                                                                                      _ti && _te; }
                                                   else return true; }
  };
  */

  return {
           restrict : 'E',
           require : {ngModel:'ngModel',form:'^form'},
           templateUrl : 'templates/directives/description.html',
           scope : {
                     description : '=ngModel',
                   }, 
           controller : controller,
           controllerAs : 'description_ctrl',
         };
}]);
