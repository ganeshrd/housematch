/**
 * Gestion i18n et L10n de l'application
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var XNSvc = function($log,LOCALE_REGEX,SUPPORTED_LANGUAGES,amMoment) {

  let locale;

  return {
    /**
     * Détermine le langage de l'utilisateur
     * Si le langage n'est pas supporté, l'anglais est choisi;
     */
    init : () => { 
                   let match = navigator.language.match(LOCALE_REGEX);
                   let user_lang = match? match[1] : 'en';
                   let lang_setup = SUPPORTED_LANGUAGES.indexOf(user_lang) >= 0? user_lang : 'en';
                   $log.debug('xn.svc.js language setup : ' + lang_setup);
                   locale = lang_setup;
                   amMoment.changeLocale(lang_setup);
                 },
    /**
     * getter
     */
    language : () => { return lacale; }
  };
};
