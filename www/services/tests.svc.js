var TestsSvc = function($log,$q,WireSvc) {

  var hm_ftp_record = (id,name,offset,action) => { return enc( tuple(atom('hm_ftp'), bin(id),bin(name),number(offset),bin(action)) ); };

  return {
    set_db_status : (state) => { dev_state.db_status = state; },
    get_db_status : () => { return dev_state.db_status; },

    /**
     * Reset de la base de donnée !! NOT IN PRODUCTION !!
     * @param scope $scope angularjs.
     * @param id L'identifiant de l'utilisateur.
     * @return promise.
     */
    clear_db : (id) => { ws.send( enc( tuple(atom('hm_dev'),atom('clear_db'), list( tuple(atom('_id'),bin(id)) ) ) )); 
                         return WireSvc.data_request('hm_dev','clear_db'); },

    /**
     * @todo 
     * Test de téléchargement.
     * Télécharge le logo HouseMatch puis retourne l'url de l'image.
     * @return promise.
     */
    download_test : (id) => { let action = 'download_test';
	                      let request = enc(tuple(atom('hm_ftp'), 
	                                              bin(''), // id
						      bin(''), // sid
						      bin(''), // name
						      list( tuple(atom('_id'),bin(id)),tuple(atom('action'),atom(action)) ), // meta
						      number(0), // total
						      number(0), // offset
						      number(0), // block
						      bin(''), // data
						      bin('file_request') )); // status
                              ws.send(request);
			      return WireSvc.ftp_receptor(action); }
  };
};
