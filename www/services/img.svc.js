/**
 * Proxy de gestion des photos de l'utilisateur.
 * i.e. sélection et création de photo
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var ImgSvc = function($cordovaCamera, $log, $q, $cordovaActionSheet, $translate) {

  /**
   * Retourne l'objet File d'un fichier.
   * @param src L'url du fichier.
   * @return promise.
   * @warning : non standard
   */
  var src_to_File = function(src) {
    let deferred = $q.defer();
    window.resolveLocalFileSystemURL(src,success,error);
    function success(fileEntry) { fileEntry.file((file) => { deferred.resolve(file); },error); };
    function error(err) { deferred.reject(err); };

    return deferred.promise;
  };

  /**
   * Retourne une image en activant le plugin cordova camera.
   * @param destType String 'DATA_URL' | 'FILE_URI' | 'NATIVE_URI'
   * @param sourceType String 'CAMERA' | 'PHOTOLIBRARY' option du plugin
   * @see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-camera/
   * @return Promise L'uri de la ressource ou une représentation base64 ou reject(false).
   */
  function do_camera(destType,sourceType) {
    let deffered = $q.defer();
    if (!window.cordova) {
      deffered.resolve("R0lGODlhEAAQAMQAAORHHOVSKudfOulrSOp3WOyDZu6QdvCchPGolfO0o/XBs/fNwfjZ0frl3/zy7////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAkAABAALAAAAAAQABAAAAVVICSOZGlCQAosJ6mu7fiyZeKqNKToQGDsM8hBADgUXoGAiqhSvp5QAnQKGIgUhwFUYLCVDFCrKUE1lBavAViFIDlTImbKC5Gm2hB0SlBCBMQiB0UjIQA7");
    } else {
      let options = {
        destinationType : Camera.DestinationType[destType],
        encodingType : Camera.EncodingType.PNG,
        sourceType: Camera.PictureSourceType[sourceType],
        quality: 75,
        targetWidth: 500,
        targetHeight: 500,
        allowEdit: false, // unpredictable behavior on android if true
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options)
        .then((src) => { deffered.resolve(src); },
              (err) => { deffered.reject(false); });
    }

    return deffered.promise;
  };

  return {

    do_camera: do_camera,

      /**
       * selectionne une image depuis le stockage de l'appareil ou prend une photo.
       * @return Promise File object.
       */
    get_picture : () => {
      console.log('img.svc.js in device block');
      let deffered = $q.defer();
      let snap_opt = $translate.instant('modal.images.0');
      let select_opt = $translate.instant('modal.images.1');
      let cancel = $translate.instant('btn_cancel');
      let options = {
                      'buttonLabels': [snap_opt, select_opt],
                      'addCancelButtonWithLabel': cancel
                    };
      $cordovaActionSheet.show(options, function (_btnIndex) {
        let cam_type = null;
        switch(_btnIndex) {
          case 1 : cam_type = 'CAMERA'; break;
          case 2 : cam_type = 'PHOTOLIBRARY'; break;
          default : return; // cancel - Is this the proper way?
        }
        do_camera('DATA_URL',cam_type).then(src => { deffered.resolve(src); }, // base64
                                            err => { $log.error('img.svc.js ' + err);
                                                      deffered.reject(err); });
      });

      return deffered.promise;
    }
  };
};
