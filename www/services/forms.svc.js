/**
 * Gestion des formulaires de recherche propriétaire ou locataire.
 * Ce service expose une API sur les données créées par l'utilisateur
 * entre les différentes vues des formulaires de recherche.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var FormsSvc = function($log,$timeout,DataSvc) {

  /**
   * Modèle vide.
   */
  let empty_form = {
                     blacklist : [],
                     charge : null,
                     description : {title : '', text : ''},
                     for : null,
                     geo : {},
                     goods : null,
                     images : [],
                     intent : '',
                     period : {timeframe : false,from : null},
                     renting : 1, // needed for the product thing (renting = PI {2;3;5})
                     timestamp : null,
                   };

  /**
   * Controller API
   */
  return {
    /**
     * Return a reseted form state
     * @params target String 'lessor' | 'tenant'
     */
    new_form : (target) => { let form = angular.copy(empty_form);
                             form.for = target;
                             return form; },

    /**
     * Envoi du formulaire
     * @param o Object Un formulaire validé
     * @return Promise
     * @see hm doc
     */
    publish : (o) => { o.timestamp = Math.round(Date.now() / 1000); // TO UNIX
                       let mandatories = {'action' : 'new_add'};
                       return DataSvc.send(mandatories,o); },
    update : (o) => { $log.debug('forms.svc.js update argument o', o);
                      let mandatories = {'action' : 'update_add'};
                      return DataSvc.send(mandatories,o); }
  };
};
