/**
 * @file register.svc.js
 * Service des inscriptions utilisateur.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var RegisterSvc = ($log,$q,CryptoSvc,DataSvc,KvsSvc) => {
  var user_data = {}; // {_id:str,surname:str,name:str,pwd:str,contacts:array}

  /**
   * Mise en forme des contacts de l'utilisateur pour l'exportation.
   * @param c Object Un éléments des contacts renvoyé par le plugin $cordovaContacts.
   * hypothèse: Seul les champs 'displayName', 'phoneNumbers' et 'emails' sont présents.
   */
  var map_contacts = (c) => {
    tels = c.phoneNumbers ? c.phoneNumbers.map(function(t) {return {'type': 'tel', 'value' : normalize(t.value)}; }) : false;
    emails = c.emails ? c.emails.map(function(e) { return { 'type' : 'email', 'value' : e.value}; }) : false;
    if(tels && emails) { contacts = tels.concat(emails); }
    else if(tels || emails) { contacts = tels || emails; }
    else { contacts = []; }

    return {'alias' : c.displayName, 'resources' : contacts};
  };

  let check_id_request = () => {
    if(_.isEmpty(user_data._id)){
      id_request().then(rcpt => {
        $log.debug("register.svc.js check_id_request -- id",rcpt.message);
        set({'id': rcpt.message});
    })
    }
  }

  /**
   * Mise en forme des numéros de téléphone
   */
  let normalize = (tel) => { let striped = tel.replace(/\s/g,'');
                             let prefix = striped.replace(/^00/,'+');
			                       return prefix.replace(/^0/,user_data.dial_code) };

  return {

    /**
     * Ré-initialisation de la variable d'instance user_data
     */
    reset_form : () => { user_data = {}; },

    /**
     * setter de la variable d'instance user_data
     */
    set : (o) => { Object.assign(user_data,o); },

    /**
     * envoi du code d'identification
     */
    id_challenge : (mandatories,args) => { let id = user_data.credentials.id;
                                           let pwd = user_data.credentials.pwd;
                                           mandatories.id = id; mandatories.pwd = pwd;
                                           return DataSvc.send(mandatories,args,false).then(resp => { return resp; }); },
    /**
     * Soumet le numéro de l'utilisateur comme identifiant
     * @return promise
     */
    id_request : (t) => {
      let pwd = CryptoSvc.gen_pass();
      let id = normalize(t);
      let mandatories = { 'action' : 'id_request', 'id' : id, 'pwd' : pwd };
      user_data.credentials = mandatories;
      return DataSvc.send(mandatories).then( resp => { return resp; });
    },

    new_sms : () => {
      let id = user_data.credentials.id, pwd = user_data.credentials.pwd;
      let mandatories = { 'action' : 'new_challenge', 'id' : id, 'pwd' : pwd };
      return DataSvc.send(mandatories,{},false).then( resp => { return resp; });
    },
    /**
     * Upload des contacts de l'utilisateur.
     */
    upload_contacts: () => {
      // prep contacts
      cnts = user_data.contacts.map(map_contacts);
      return DataSvc.send({'action':'upload_contacts'},{ 'contacts' : cnts });
    },

    /**
     * Sauvegarde les paramètres d'identification
     */
    store_credentials : () => { let credentials = user_data.credentials;
                                delete credentials.action;
                                return KvsSvc.store_object(credentials);
    }
  };
};
