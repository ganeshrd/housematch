/**
 * @file localisatio.svc.js
 * Service recuperation localisation.
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */

var LocalisationSvc = ($q) => {

  return {
    getPosition: () => {
      let deferGetlocalisation = $q.defer();
      let geolocationSuccess = (position) => {
          var Geocoder = new google.maps.Geocoder;
          function geocodeLatLng(latlng, cb) {
            return Geocoder.geocode({
              'location': latlng
            }, function(res, status) {
              return cb((status === 'OK') ? (res[1] || "Nothing found") : status);
            });
          }
          var coordinates = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
          geocodeLatLng(coordinates, function(data) {
            _.forEach(data.address_components, (adr) => {
              let short_name = adr["short_name"];
              if (short_name && short_name.length == 2) {
                deferGetlocalisation.resolve(short_name);
              }
            })
          });
      }

      let geolocationError = () => {
        deferGetlocalisation.reject("");
      }


      navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
     return deferGetlocalisation.promise;
    }
  };
};
