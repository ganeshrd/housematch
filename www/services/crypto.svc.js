/**
 * Génération des valeurs secrètes
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 */
var CryptoSvc = ($log,md5,KvsSvc) => {
  let pass = () => { let a = new Uint16Array(10), pass;
                     window.crypto.getRandomValues(a);
		                 for(let i=0;i< a.length;i++) { pass += a[i].toString(); }
                      
		                 return md5.createHash(pass); };
  return {
    /**
     * Génération d'un mot de passe
     */
    gen_pass : pass
  };
};
