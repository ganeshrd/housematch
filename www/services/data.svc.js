/**
 * Service de transmission des données discrètes
 * @author Renaud Tripathi
 */
var DataSvc = ($log,$state,$q,$ionicLoading,CryptoSvc,WireSvc,KvsSvc,ModalsSvc)  => {

  var cache = {};

  /**
   * bert encoder wrapper
   * encode une liste à partir d'un array
   */
  let array_to_list = (a) => { if(a) return list.apply(null,a);
                               else return list(); };

  /**
   * Processus de retrait du résultat de requête.
   * @param id_transaction String l'identifiant de la transaction
   * @param request La requete encodé au format BERT.
   */
  let get_result = (id_transaction,request) => {
    return WireSvc.transaction(id_transaction,request).then( r => { if(r.status == 'acknowledge')
                                                                    {
                                                                      let prev = cache.request;
                                                                      let action = next_action(prev.data.mandatories.action);
                                                                      let m = { 
                                                                                'action' : action,
                                                                                'id' : prev.data.mandatories.id, 
                                                                                'pwd' : prev.data.mandatories.pwd
                                                                              };
                                                                      return _retreive(id_transaction,m);
                                                                    }
                                                                    else // all other status 
                                                                    {
                                                                      $ionicLoading.hide(); return r;
                                                                    }
                                                                  },
                                                             e => { $ionicLoading.hide(); return network_failure(); });
  };

  /**
   * Formalisation BERT de la requête et mise en mémoire cache.
   * @param t String identifiant de transaction.
   * @param m Object les valeurs obligatoires de la requête.
   * @param a Object Objet les arguments de la requête.
   * @return payload La requête mise en forme selon le protocole BERT.
   */
  let mk_request = (t,m,a) => { let request = {
                                                'id_t' : t,
                                                'data' : { 'mandatories' : m, 'args' : a }
                                              };
                                $log.info('data.svc.js request is',request);
                                let payload = object_to_proplist(request); payload = enc(payload);
                                save_request(t,request,payload);

				                        return payload;
  };

  /**
   * fonction générique de traitement des ruptures de réseau
   * Déclenche une popup proposant de relancer l'opération courrante
   * ou de quitter l'application.
   */
  let network_failure = () => {
    return ModalsSvc.network_failure()
      .then( res => { if(res) return retry(); },
             err => { $log.error('data.svc.js network_failure',err); $state.go('system_failure'); });
  };

  /**
   * Retourne un nouvel identifiant de transaction.
   * @param id String l'identifiant de l'utilisateur.
   * @return String.
   */
  let new_transaction_id = (id) => { return id + '-' + CryptoSvc.gen_pass(); };

  /**
   * Détermine la valeur d'action suivante
   * $param action = l'action précédente
   * @return String
   */
  let next_action = (prev) => { let action;
                                switch(prev) {
                                  case 'zombie_mode' : action = 'zombie_package'; break;
                                  case 'new_challenge':
                                  case 'id_challenge':
                                  case 'id_request' : action = 'retrieve_cl'; break;
                                  default : action = 'retrieve'; break;
                                }

                                return action;
                              };
  /**
   * Encodage des requêtes
   */
  let object_to_proplist = (o) => {
    let a = _.entries(o);
    let struct_keys = [ 'mandatories','args','data' ];
    let object_keys = [ 'description','period','add_state','geo','coordinates','status' ];
    let object_list = [ 'contacts','resources' ];
    let atom_keys = [ 'id_t','id','pwd' ];
    let atom_values = [ 'timeframe' ];
    let atom_key_value = [ 'action' ];
    let num_values = [ 'timestamp','charge','from','to','renting','age','floor','rooms','beds','goods', 'challenge','last' ];
    let bin_list = [ 'images', 'blacklist' ];
    let value_to_string = [ 'latitude','longitude' ];
    let prop = a.map(function(t) { 
      if(struct_keys.indexOf(t[0]) >= 0) return tuple( atom(t[0]),object_to_proplist(t[1]) );
      else if(object_keys.indexOf(t[0]) >= 0) return tuple( bin(t[0]),object_to_proplist(t[1]) );
      else if(object_list.indexOf(t[0]) >= 0) return tuple( bin(t[0]),array_to_list( t[1].map(c => { return object_to_proplist(c); }) ));
      else if(atom_keys.indexOf(t[0]) >= 0) return tuple( atom(t[0]),bin(t[1]) );
      else if(atom_values.indexOf(t[0]) >= 0) return tuple( bin(t[0]),atom(t[1]) );
      else if(atom_key_value.indexOf(t[0]) >= 0) return tuple( atom(t[0]),atom(t[1]) );
      else if(num_values.indexOf(t[0]) >= 0) return tuple( bin(t[0]),number(parseInt(t[1],10)) ); // ensure number
      else if(bin_list.indexOf(t[0]) >= 0) return tuple( bin(t[0]), array_to_list(t[1].map(s => { return bin(s); })) );
      else if(value_to_string.indexOf(t[0]) >= 0) return tuple( bin(t[0]), bin(t[1].toString()) );
      else return tuple( bin(t[0]),bin(t[1]) ); });

    return array_to_list(prop);
  };

  /**
   * Renvoi de la dernière requête.
   * Utilise les données stockées dans la variable d'instance 'cache'
   * @return Promise
   */
  let retry = () => { let id_trans = cache.id_t, request = cache.payload;
                       ws.send(request);
                       $ionicLoading.show();
                       return get_result(id_trans);
  };

  /**
   * Mise en cache de la requête
   * @param i String Identifiant de transaction.
   * @param r Object Données de la requête.
   * @param p Object Données encodées.
   */
  let save_request = (i,r,p) => { cache.id_t = i; cache.request = r; cache.payload = p; };

  /**
   * Transmission des données.
   * Cette méthode est toujour appelée depuis l'api du service depuis un controlleur.
   * @param mandatories Object les données obligatoire de la requête. Ne contient que
   * la propriété 'action' au moment de l'appelle.
   * @param opt Object Les arguments de la requête.
   * @param nt Boolean si vrai (defaut) send demarre une nouvelle transaction.
   * @return Promise
   */
  let _send = (mandatories,opt,new_t) => {
      $log.debug('data.svc.js _send arguments',mandatories,opt,new_t);
      opt = opt || {};
      new_t = angular.isDefined(new_t)? new_t : true;
      if(['id_request','id_challenge','new_challenge'].indexOf(mandatories.action) >= 0)
      {
        let id_t = new_t? new_transaction_id(mandatories['id']) : cache.id_t;
        let request = mk_request(id_t,mandatories,opt);
        ws.send(request);
        $ionicLoading.show();
        return get_result(id_t);
      }
      else
      {
         return  KvsSvc.get_id_pwd().then(o => { $log.debug('data.svc.js promise o',o);
                                                 mandatories['id'] = o['id']; mandatories['pwd'] = o['pwd'];
                                                 let id_t = new_t? new_transaction_id(mandatories['id']) : cache.id_t;
                                                 let request = mk_request(id_t,mandatories,opt);
                                                 ws.send(request);
                                                 $ionicLoading.show();
                                                 return get_result(id_t);
                                               },
		                                      e => { $log.error('data.svc.js kvs failure',e); $state.go('system_failure'); } );
      }
  };

  /**
   * Envoie une requête de retrait d'information avec l'ensemble des champs mandatories pré-remplis.
   * @param id_t String L'identifiant de transaction.
   * @param mandatories Object La partie obligatoire d'une requête.
   * @param args Object La partie optionnelle d'une requête.
   * @return Promise
   */
  let _retreive = (id_t,mandatories,args) => {
      args = args || {};
      let request = mk_request(id_t,mandatories,args);
      ws.send(request);
      $ionicLoading.show();
      return get_result(id_t,request);
    };

  return {
    /**
     * Retourne vrai si id et pwd sont présent dans la base de donnée
     */
    is_registered: () => { return $q.all([KvsSvc.get_store_value('id'),KvsSvc.get_store_value('pwd')]).then(p => { return true; }); },
    send: _send
  };
};
