/**
 * Interface N2O / angularjs
 * Emulateur de cycle 'digest' appelé ici 'hm_digest'.
 * La fonction 'get_link' retourne l'interface du service. Elle sert à affecter la 
 * variable globale hm_bridge.
 */
var WireSvc = function($q,FTP_TIMEOUT,WS_TIMEOUT,MAX_ATTEMPT,$timeout,$log) {
  
  /** 
   * Si aucune clé de déclenchement n'est présente au bout de MAX_ATTEMPT cycles, la promesse est rejetée.
   */
  var receptor = {};
  var reset_receptor = () => { receptor = {}; };

  /** 
   * Test d'existence de propriété de l'objet receptor.
   * @param id_transaction String L'identifiant de la transaction.
   * @return boolean.
   */
  var result = (id_transaction) => { return receptor.hasOwnProperty(id_transaction); };

  /**
   * Effectue un 'hm_digest' sur une propriété de l'objet receptor.
   * @param id_transaction String L'identifiant de la transaction.
   * @param request la requête au format BERT
   * @param deferred Une instance Deferred.
   * @param count Integer Nombre de tentative de résolution.
   * @return Promise.
   */
  var watch = function(id_transaction,request,deferred,count) {
    if(count == 0) { deferred.reject(id_transaction); return deferred.promise; }
    if(result(id_transaction)) 
    { 
      let payload = receptor[id_transaction];
      if(payload.status != 'not_yet') 
      {
        deferred.resolve(payload); 
        return deferred.promise;
      }
      else
      {
        ws.send(request); // retry
      }
    }
    return $timeout(watch,WS_TIMEOUT,false,id_transaction,request,deferred,count-1);
  };

  /**
   * Réponse serveur, affectation de la variable receptor.
   * @param id_transaction String L'identifiant de la transaction.
   * @param value JSON Les données de la réponse.
   */
  var data_transfer = (id_transaction,value) => { receptor[id_transaction] = value; };

  return {

    /**
     * Retourne l'interface du service.
     * Cette méthode sert à affecter la variable globale hm_bridge.
     * Ell n'est appelée qu'une seule fois lors du démarage de l'application.
     */
    link : () => { hm_bridge = { 'data_transfer' : data_transfer }; receptor = {}; },

    /**
     * Effectue un 'hm_digest' sur une propriété 'id_transaction' de l'objet receptor.
     * @param id_transaction L'identifiant de la transaction.
     * @param request La requête au format BERT.
     * @return promise
     */
    transaction : (id_transaction,request) => { reset_receptor();
                                                return watch(id_transaction,request,$q.defer(),MAX_ATTEMPT); }
  };
};
