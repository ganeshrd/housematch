var PermissionSvc = ($q, $ionicPlatform) => {

  return {
    requestRemoteNotificationsAuthorization: () => {
      let deferGetPermission = $q.defer();
      if(window.cordova) {
        cordova.plugins.diagnostic.requestRemoteNotificationsAuthorization({
          successCallback: () => {
            deferGetPermission.resolve();
          },
          errorCallback: (err) => {
            deferGetPermission.reject();
          },
          types: [
              cordova.plugins.diagnostic.remoteNotificationType.ALERT,
              cordova.plugins.diagnostic.remoteNotificationType.SOUND,
              cordova.plugins.diagnostic.remoteNotificationType.BADGE
          ],
          omitRegistration: false
        });
      } else {
        deferGetPermission.reject();
      }
      return deferGetPermission.promise;
    },
    redirectSettings: (Permission) => {
      if(window.cordova){
        cordova.plugins.diagnostic.switchToSettings();
      }
    },
    requestContactsAuthorization : () => {
      let deferGetPermission = $q.defer();
      $ionicPlatform.ready(function() {
        if(window.cordova){
          cordova.plugins.diagnostic.requestContactsAuthorization((status) => {
            if(status === cordova.plugins.diagnostic.permissionStatus.GRANTED){
              deferGetPermission.resolve();
            } else {
              deferGetPermission.reject();
            }

          }, function(error){
              deferGetPermission.reject();
          });
        } else {
          deferGetPermission.resolve();
        }
    });
    return deferGetPermission.promise;
    }
  };
};
