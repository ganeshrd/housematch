/**
 * API des modales HouseMatch
 *
 * @author Renaud Tripathi <rkdt.ganeshrd@gmail.com>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */

var ModalsSvc = function($log,$ionicPopup,$ionicModal,$translate, $q) {

  var confirm_template = { okType : 'button-positive',cancelType : 'button-stable',cssClass: 'hm-confirm'};
  var network_failure = { okType : 'button-positive',cancelType : 'button-stable',cssClass: 'hm-network-failure'};

  /**
   * Fonction générique de traitement des modales
   * @param template String L'url du template passé en argument à la fonction $ionicModal.fromTemplateUrl
   * @param opt Object Argument optionnels passés à la fonction $ionicModal.fromTemplateUrl
   */
  var displayTemplate = function (template,opt, scope) {
    let defer = $q.defer()
    opt = opt || {};
    let args = Object.assign({scope: scope, animation : 'slide-in-down'},opt);
    $ionicModal.fromTemplateUrl(template, args)
      .then( modal => {
              defer.resolve(modal);
              scope.modal = modal;
              self.modal = modal;
              self.modal.show();
	     });
    return defer.promise;
  };


  /**
   * Fonction générique de traitement des modales
   * @param template String L'url du template passé en argument à la fonction $ionicModal.fromTemplateUrl
   * @param opt Object Argument optionnels passés à la fonction $ionicModal.fromTemplateUrl
   */
  var do_modal = function (template,scope) {
    scope = scope || null;
    let args = Object.assign({'scope': scope,animation : 'slide-in-down'});

    return $ionicModal.fromTemplateUrl(template, args)
             .then( modal => { return modal; });
  };

  /**
   * Service API
   */
  var displayAboutHM = function () {
    displayTemplate('templates/modals/aboutHM.html')
  };
  // var displayRateHm = function () {
  //   // displayTemplate('templates/modals/rateHm.html')
  //   do_modal('templates/modals/rateHm.html');
  // };

  var succesForm = function (scope) {
    displayTemplate('templates/modals/succesForm.html', {}, scope)
  };

  var displayPermissionWarnnig = function (scope) {
    displayTemplate('templates/modals/permission-warnnig.html', {}, scope)
  };
  var chooseMedia = (scope) => {
    return displayTemplate('templates/modals/choose-media.html', {}, scope)
  };
  var cropImg = (scope, url, save) => {
    return displayTemplate('templates/modals/crop-img.html', {}, scope)
  };

  /**
   * USER API
   */
  return {
    rateHm : function() { return do_modal('templates/modals/rateHm.html'); },
    cgu : function() { return do_modal('templates/common/CGU.html'); },
    privacy : function() { return do_modal('templates/common/politicConfident.html'); },
    adds_edit : function(opt,scope) { return do_modal('templates/modals/adds_edit.html',opt,scope); },
    displayAboutHM: displayAboutHM,
    displayPermissionWarnnig: displayPermissionWarnnig,
    chooseMedia: chooseMedia,
    cropImg: cropImg,
    succesForm: succesForm,
    hm_confirm : function(o) { return $ionicPopup.confirm(Object.assign(confirm_template,o)); },
    network_failure : function() {
                                   let o = {
                                             title : $translate.instant("modal.network_failure.0"),
                                             template : $translate.instant('modal.network_failure.1'),
                                             okText : $translate.instant('btn_retry'),
                                             cancelText : $translate.instant('btn_abort')
                                           };
                                    return $ionicPopup.confirm(Object.assign(network_failure,o));
                                  },
  };
};
