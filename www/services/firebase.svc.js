/**
 * @file firebase.svc.js
 * Service de stockage et d'accès aux photos de l'utilisateur.
 * Utilise le service externe google firebase
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 * @author Idriss Abouelaiz <idriss.abouelaiz@gmail.com>
 */
var FirebaseSvc = ($log, $q, FIREBASE_PARAMS, NotificationSvc) => {

  // var app, storage_svc = null, isAnonymous, uid; ??
  let app = null, storage_svc = null, isAnonumous = null, uid  = null, base_ref = null;

  /**
  * Primitive de téléchargement
  * @param src String base64
  * @param ref Reference référence de stockage google store
  * @return Promise
  */
  function _upload(ref,src) {};
  /**
  * Upload les images de l'utilisateur
  * @param images Array base64
  * @return Promise Un tableau des references des images stockées
  */
  function store(images) {
                           if(images == undefined)
                           { // mock
                             p = $q.defer(); p.resolve([]); return p.promise;
                           }
                           $log.debug('firebase.svc.js:store base_ref: ' + base_ref.bucket);
                           let promises = images.map(img => {
                             let ref = base_ref.child(uid + '/' + Date.now() + '.png');
                             return ref.putString(img.src,'base64')
                               .then( res => { $log.debug('firebase.svc.js resolve : ' + res.ref.fullPath);
                                               return res.ref.fullPath; },
                                      err => { $log.error('firebase.svc.js ' + err); }); });

                           return $q.all(promises);
   };
   /**
    * Mise à jour des images d'une annonce
    * @param Array deleted La liste des références à supprimer.
    * @param Array new_refs La Liste des images au format base64 à ajouter.
    * @return Promise
    */
   function update(deleted,new_refs) {
                                       let promises = deleted.map(img => {
                                                                           let ref = base_ref.child(img);
                                                                           return ref.delete(); });
                                       return $q.all(promises).then(ps => { return store(new_refs); });
   };

   /**
    * Uploader les images en format base64
    * @param string image to upload
    * @return Promise
    */
   function uploadBase64(imgb64Data) {
     let deffer = $q.defer();
     firebase.storage().ref().child('avatar')
     .putString(imgb64Data.substring(imgb64Data.indexOf(',')+1), 'base64', {contentType:'image/jpg'})
     .then(function(snapshot) {
      deffer.resolve(snapshot)
    });
    return deffer.promise;
   };

  return {

           /**
           * Firebase app initialisation
           * Only once
           * Assign variable instances app, storage_svc, isAnonimous and uid
           */
           initialize: (email) => {
                               $log.debug('storage.svc.js intialize()');
                               // retrieve app interface
                               app = firebase.initializeApp(FIREBASE_PARAMS);
                                firebase.auth().signInWithEmailAndPassword('database@housemutch.com', 'housemutch');
                               firebase.auth().onAuthStateChanged(function(user) {
                               if (user) {
                                 // User is signed in.
                                 isAnonymous = user.isAnonymous;

                                 uid = user.uid;
                                 window.localStorage.setItem("uid", uid);
                                 // alert('uid' + uid);
                                 NotificationSvc.init(uid);
                                 // alert('uid' + uid);
                                 // storage service interface assignment
                                 storage_svc = app.storage();
                                 base_ref = storage_svc.ref();
                               } else { } // user log out
                               });
           },

           /**
           * Téléchargement des images de l'annonce.
           * @param les références (enfant) des images dans l'espace de stockage.
           * @return Promise Un tableau des url.
           */
           download : (refs) => {
                                  $log.debug('storage.svc.js download()');
                                  let promises = refs.map(ref => {
                                    if(!base_ref) {
                                      app = firebase.initializeApp(FIREBASE_PARAMS);
                                      storage_svc = app.storage();
                                      base_ref = storage_svc.ref();
                                    }
                                    return base_ref.child(ref).getDownloadURL()
                                          .then(url => { return {'src':url,'ref':ref}; });
                                        });
                                  return $q.all(promises);
           },
           store : store,
           update : update,
           uploadBase64: uploadBase64,
           uid: () => {
             let val = _.get(firebase.auth(), 'currentUser.uid');
             return val || window.localStorage.key('uid');
           },
           databaseRef: () => {
             return firebase.database().ref()
           },
           /**
           * Retourne une configuration actionsheet
           * @param k String Un clé du dicionnaire de configuration actionsheet
           * @return Object
           */
           as : (k) => { return as[k]; }
         };
};
