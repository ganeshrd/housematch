/**
 * Mise en cache des données de l'utilisateur pour une session d'utilisation.
 * La mise en cache intervient dès l'authentification.
 * Sont sauvegardé: le profil, la messagerie, les annonces et l'horodatage de connexion.
 *
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var SessionSvc = ($log,$cacheFactory,$state,DataSvc) => {

  var cache = $cacheFactory('cache');

  return {

    /** getters */
    user_profile : () => { return cache.get('profile'); },
    user_adds : () => { return cache.get('adds'); },

    /** setters */
    set_user_state : (state) => { 
                                  cache.put('adds',state.adds);
				                          cache.put('profile',state.profile);
                                },
    refresh : () => {
                      return DataSvc.send({'action':'user_state'})
                        .then(resp => { let adds = resp.value.adds;
                                        let profile = resp.value.profile;
                                        cache.put('adds',adds);
                                        cache.put('profile',profile);
                                        return true; })
                        .catch(err => { $log.error('session.svc.js err',err);
                                        $state.go('system_failure'); }); }

  };
};
