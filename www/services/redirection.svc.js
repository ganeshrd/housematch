/**
 * Service gestion de la Redirection.
 *
 */
var RedirectionSvc = (DataSvc, KvsSvc) => {

  return {
    /**
     * Retourne state de la redirection.
     * @return promise
     */
    checkRedirection : () => {

      return KvsSvc.get_id_pwd().then((response) => {
        console.log('response', response);
        return  DataSvc.send({action: 'user_state'}).then((res) => {
          let adds = _.get(res, 'value.adds');
          console.log('res >>>>>>>>>>>>>>>>>', adds);
          let hasToAddRegisterNumber = false;
          let hasRequest = false;
          let connectionCount = window.localStorage['connectionCount'] || 0;
          let num =  Number(connectionCount);
          if(isNaN(num)) {
            num = 1;
          }
          window.localStorage['connectionCount'] = num + 1;
          if (!response) {
            return 'tutorial';
          }
          if (response.message == "no_session") {
            return 'tutorial';
          }
          _.forEach(adds, add => {
            if(add.for == "tenant" && moment().isAfter(moment(add.timestamp).add(-5, 'days')) && !add['add_state']) {
              hasToAddRegisterNumber = true;
            } else if (add.for == "lessor") {
              hasRequest = true;
            }
          })
          if(hasToAddRegisterNumber) {
            return 'register-number';
          } else if (hasRequest && num >= 10 && (num%2 == 0)) {
            return 'share';
          }
          return 'nav.news';
        });

      }, () => {
        return 'tutorial';
      })
    }
  };
};
