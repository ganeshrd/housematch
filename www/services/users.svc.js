/**
 * Service des utilisateur.
 *
 */
var UsersSvc = ($log,DataSvc, $q, SessionSvc, FirebaseSvc,KvsSvc) => {

  return {
    /**
     * Retourne les utilisateurs de la base de données.
     * Cette méthode utilise une méthode dépréciée du service DataSvc.
     * @return promise
     *  * SUPPRIMER EN PRODUCTION
     */
    get_users : () => {
      return KvsSvc.store_object({id:'no_id',pwd:'no_pwd'}).then(r => { return DataSvc.send({action:'zombie_mode'}); })
        .catch(angular.noop);
    },
    current_user: null,
    saveUserAttr: (attr, val) => {
      let accountKey = '/accounts/' + FirebaseSvc.uid();
      FirebaseSvc.databaseRef().child(accountKey + '/' + attr).set(val);
    },

    getMembers: (tel) => {
      let deferred = $q.defer();
      firebase.database().ref().child('telUidMap/' + tel).once("value").then((snap) => {
        console.log('getUser snap.val()', snap.val());
        if (snap.val()) {
          firebase.database().ref().child('accounts/' + snap.val()).once("value").then((snap2) => {
            let user1 = snap2.val();
            if(user1) {
              user1.uid = snap.val();
              user1.name = user1.name || 'nomInconue';
              user1.surname = user1.surname || 'prenomInconue';
              firebase.database().ref().child('accounts/' + FirebaseSvc.uid()).once("value").then((snap3) => {
                let user2 = snap3.val();
                user2.name = user2.name || 'nomInconue';
                user2.surname = user2.surname || 'prenomInconue';
                user2.uid = FirebaseSvc.uid();
                deferred.resolve([user1, user2]);
              });

            } else {
              deferred.reject();
            }
          });
        } else {
          deferred.reject();
        }

      });
      return deferred.promise;
    },
    /**
     * Retourne le profile de l'utilisateur en cours.
     * @return promise
     */
    get_current_user() {
      let deferred = $q.defer();
      let profile = SessionSvc.user_profile();
      console.log('get_current_user SessionSvc.user_profile', profile);

      if (profile) {
        deferred.resolve(profile);
        this.current_user = profile;
      } else {
        DataSvc.send("hm_client", 'get_profile').then((response) => {
          if(response.name) {
            deferred.resolve(response);
            this.current_user = profile;
          } else {
            console.log('get_current_user error');
            deferred.reject();
          }
        })
      }
      return deferred.promise;
    }

  };
};
