var DbSvc = function(DATABASE,DB_SIZE,$cordovaSQLite,$q,$ionicPlatform,$log) {

  var hm_conn = null;
  var kvs_stmt = "CREATE TABLE IF NOT EXISTS kvs (" +
                 "k text primary key not null," +
		             "v text not null )";
  var adds_stmt = "CREATE TABLE IF NOT EXISTS adds (" +
                  "_id text primary key not null," +
          		    "geo text not null, " +
          		    "goods text not null, " +
          		    "'from' text not null, " +
          		    "period text not null, renting text not null, " +
          		    "charge text not null, for text not null," +
          		    "timestamp text not null," +
          		    "images text not null," +
          		    "blacklist text not null," +
          		    "status text not null," +
          		    "description text not null )";

  /**
   * Création des tables de l'application
   * @param reset Boolean Si vrai écrase les tables existantes.
   */
  var create_tables = (reset) => {
    let queries = [ kvs_stmt, adds_stmt ];
    if(reset == true) queries = [ "drop table if exists kvs","drop table if exists adds" ].concat(queries);
    hm_conn.sqlBatch(queries)
      .then(() => { $log.info('tables created'); })
      .catch(e => { $log.error('db.svc.js ' + e); });
  };

  /**
   * Affecte la variable d'instance 'hm_conn'.
   * @param reset Boolean Si vrai écrase les tables existantes.
   */
  var db_init = (reset) => {
    hm_conn = new $cordovaSQLite();
      if(window.cordova){ // device
        hm_conn.openDatabase({name: DATABASE,location:"default"})
          .then(() => { $log.info(DATABASE + ' database service initialized'); 
                        create_tables(reset); })
          .catch(e => { $log.error('db.svc.js ' + e); });
      } else { // browser
        hm_conn.openDatabase(DATABASE,"0.1","hm beta db",1024)
          .then(() => { $log.info(DATABASE + ' database service initialized');
                        create_tables(reset); })
          .catch(e => { $log.error('db.svc.js ' + e); return; });
      }
  };
  
  /**
   * @private
   * Execute une requête SQLite au sein d'un transaction.
   * @param stmt Le corps de la requête.
   * @param args Les arguments de la requête fournis sous forme d'un tableau.
   * @return Promise.
  var exec = function(stmt,args) {
      args = args || [];
      let q = $q.defer();
      $ionicPlatform.ready(function(){
      let db = connect();
      db.transaction(function(tx) {
        tx.executeSql(stmt,args, 
	              function(tx,results) {
		        q.resolve(results);
		      },
		      function(tx,error) {
		        $log.error('DbSvc error ',stmt, args,hm.dbg_object(error));
		        q.reject(error);
		      });
      });});
      return q.promise;
    };
   */

  return {

    /**
     * Initialistation du service de base de données.
     * Cette méthode ne doit êter appelée qu'une seule fois lors 
     * de l'initialisation de l'application.
     */
    init : db_init,

    /**
     * Requetage de la base de donn\'ees.
     * hypothèse $ionicPlatform.ready() == true
     * @param stmt String Une requête SQlite.
     * @param args Array Les arguments de la requête.
     * @return promise.
     */
    query : (stmt,args) => {
      args = args || [];
      let q = $q.defer();
      hm_conn.executeSql(stmt,args, results => { q.resolve(results); },
		                                error => { $log.error('DbSvc error ',stmt, args,hm.dbg_object(error));
		                                           q.reject(error); });
      return q.promise;
    },

    /**
     * retourne une interface de connexion à la base de données
     * Le service doit avoir été initialisé au préalable par l'appelle
     * à la méthode 'db_init'
     */
    conn : () => { return hm_conn; }

    /**
     * @private
     * Efface les entrées de la table kvs.
     * Cette fonction doit être supprimée en production.
    reset_kvs: function() {
      stmt = "delete from kvs";
      return exec(stmt,[]);
    }
     */
  };
};
