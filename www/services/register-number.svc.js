/**
 * Service gestion du numero d'enregistrement.
 *
 * Ce service est appelé juste après une authentification réussie.
 * Les annonces de l'utilisateur sont donc disponible depuis SessionSvc.
 */
var RegisterNumberSvc = (SessionSvc) => {

  return {
    check_redirection : () => {
      return _.some(SessionSvc.user_adds(), add => {
          return add.for == "tenant" && 
                 moment().isAfter(moment(add.timestamp).add(-5, 'days')) && 
                 !angular.isDefined(add.state);
        });
    }
  };
};
