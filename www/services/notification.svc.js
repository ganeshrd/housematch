var NotificationSvc = (PermissionSvc) => {

  return {
    init: (uid) => {
      PermissionSvc.requestRemoteNotificationsAuthorization().then(() => {
        let push = window.PushNotification.init({
           ios: {
             alert: "true",
             badge: true,
             sound: 'false'
           }
         });

       push.on('registration', function(data) {
         firebase.database().ref('accounts/' + uid + '/token').set(data.registrationId, (error) => { });
       });
      });
    }
  };
};
