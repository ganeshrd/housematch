/**
 * @deprecated
 * Service d'accès RW à la table des annonces utilisateur 'adds'.
 * @author Renaud Tripathi
 */
var AddsSvc = function(DbSvc,DataSvc,$log) {

  var string_keys = [ 'geo', 'description', 'images', 'blacklist' ];

  return {
    /**
     * Ajoute une nouvelle entrée à la table SQLite 'adds'.
     * Construction dynamique de la requête d'insertion.
     * @param o Object Un objet décrivant une annonce utilisateur.
     * typiquement la valeur de $scope.form du controlleur requérant.
     */
    insert_add : (o) => {
      // let e = Object.entries(o); // unrecognised (xwalk ?)
      let e = _.toPairs(o);
      //$log.debug(hm.dbg_object(e));
      let args = e.reduce( (acc,val) => 
                   {
                     let k = val[0], v = val[1];  
		     if(string_keys.indexOf(k) >= 0) v = JSON.stringify(v);
	             // if(k == 'geo' || k == 'description') v = JSON.stringify(v);
	             acc.stmt += "'" + k + "'" + ','; acc.values.push(v);
	             return acc;
                   },
	           {stmt : 'INSERT INTO adds(', values : []} );
      let stmt = (args.stmt.substring(0,args.stmt.length -1)) + ') VALUES (';
      stmt += '?,'.repeat(e.length);
      stmt = stmt.substring(0,stmt.length -1) + ')';
      
      //$log.debug(stmt);
      //$log.debug(hm.dbg_object(args.values));

      return DbSvc.query(stmt,args.values);
    },

    /**
     * Retourne l'ensemble des annonces de l'utilisateur.
     * @todo synchronisation table adds et serveur
     */
    get_adds : () => { return DataSvc.send('hm_client','get_adds'); }
  };
};
