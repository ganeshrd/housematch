/**
 * Service d'authentification des utilisateurs
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var AuthSvc = ($q,$log,$state,DataSvc,KvsSvc,SessionSvc) => {

  return {

    /**
     * Authentification des utilisateurs.
     * @param id String L'identifiant de l'utilisateur tel que retourné par KvsSvc. Optionnel.
     * @param pwd String Le mot de passe de l'utilisateur tel que retourné par KvsSvc. Optionnel.
     * Si au moins un des deux argument vaut 'undefined', Les valeurs de 'id' et 'pwd' sont recherchées
     * dans la table kvs.
     * L'utilisation des paramètres est proscrite. Elle n'est utile qu'en mode developpement.
     * @return promise
     */
    authenticate : (id,pwd) => {
      if(typeof(id) == 'undefined' || typeof(pwd) ==  'undefined')
      {
        // $log.info('auth.svc.js Auth args undefined. Fetching in database');
        return $q.all([KvsSvc.get_store_value('_id'),KvsSvc.get_store_value('pwd'), KvsSvc.get_store_value('user')])
                 .then( p => { let id = p[0], pwd = p[1], user = p[2];
                              console.log('authenticate user', user.uid);
	                             DataSvc.send('hm_auth','authenticate',{'pwd':pwd})
				                         .then( receipt => {
                                     // $log.debug('auth.svc.js ' + hm.dbg_object(receipt.data));
                                     // console.log('>>>>>>> receipt', receipt.data);
                                     SessionSvc.set_user_state(receipt.data);
                                   //irebase.auth().si
                                     /*
                                     firebase.auth().signInWithEmailAndPassword(receipt.data.profile.email,'housemutch')
                                     .then(() => {
                                       console.log('>>>>>>  success');
                                     })
                                     .catch(function(error) { $log.error('firebase.svc.js',error.code,error.message); });
  */
                                   return receipt;
                               });
                 });
      }
      else
      {
        // for tests only
        $log.warn('auth.svc.js arguments provided for authentication. Using deprecated function.');
        return DataSvc.data(id,'hm_auth','authenticate',{'pwd':pwd})
	         .then( receipt => {
                               SessionSvc.set_user_state(receipt.data);
				                       return receipt;
                             });
      }
    }
  };
};
