var DatepickerSvc = function($log,$translate) {

  let weekDaysList = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
  let monthList = ["janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];
  let hm_params = {
    templateType: 'MODAL', // POPUP   | MODAL
    modalFooterClass: 'bar-clear',
    header: 'Dates',
    headerClass: 'balanced-bg dark',
     
    btnsIsNative: false,
     
    btnOk: 'Valider',
    btnOkClass: 'button-outline button-balanced',
     
    btnCancel: 'Annuler',
    btnCancelClass: 'button-clear button-dark',
     
    btnTodayShow: false,
    btnToday: 'Aujourd\'hui',
    btnTodayClass: 'button-clear button-dark',
     
    //btnClearShow: true,
    btnClear: 'Clear',
    btnClearClass: 'button-clear button-dark',
     
    selectType: 'MULTI', // SINGLE | PERIOD | MULTI
     
    tglIndeterminatePeriodShow: true, // true | false (default)
    tglIndeterminatePeriod: 'Période indéterminée',
    isIndeterminatePeriod: true, // true (default) | false
    tglIndeterminatePeriodClass: 'toggle-balanced',
    titleIndeterminatePeriodClass: 'dark',
    
    tglSelectByWeekShow: false, // true | false (default)
    tglSelectByWeek: 'Semaines entières',
    isSelectByWeek: false, // true (default) | false
    selectByWeekMode: 'NORMAL', // INVERSION (default), NORMAL
    tglSelectByWeekClass: 'toggle-positive',
    titleSelectByWeekClass: 'positive positive-border',
     
    accessType: 'WRITE', // READ | WRITE

    disabledDates: [],
     
    conflictSelectedDisabled: 'DISABLED', // SELECTED | DISABLED
     
    closeOnSelect: false,
     
    mondayFirst: true,
    weekDaysList: weekDaysList,
    monthList: monthList,
  };
  return {
    params : function() { return hm_params; }
  };
};
