/**
 * Service gestion messages.
 *
 */
var MessageSvc = (FirebaseSvc, FIREBASE_PARAMS, SessionSvc, $q, DataSvc, UsersSvc) => {
  let vm = this;
  let getDetailRoom = (key) => {
    let deferred = $q.defer();
    let ref = '/rooms/' + key;
    console.log('getDetailRoom ref', ref);
    firebase.database().ref().child(ref).once('value', snap => {
      let room = snap.val();
      room.key = key;
      deferred.resolve(room);
    });
    return deferred.promise;
  };

  return {
    /**
     * add message
     */
    add : () => {
      let app = firebase.initializeApp(FIREBASE_PARAMS, $q);

      console.log('FirebaseSvc', app);
    },
    getMessages: () => {
      let deferred = $q.defer();
      let promises = [];
      let ref = '/accounts/' + FirebaseSvc.uid() + '/rooms';
      firebase.database().ref().child(ref).once('value', snap => {
        console.log('val', snap.val());
        let rooms = snap.val();
        _.forEach(rooms, room => {
          console.log('room', room);
          promises.push(getDetailRoom(room));
        });
        $q.all(promises).then( response => {
          deferred.resolve(response);
        });
      });
      return deferred.promise;
    },
    getRoom : (tel) => {
      let deferred = $q.defer();
      let accountKey = '/accounts/' + FirebaseSvc.uid();
      console.log('getRoom accountKey', accountKey);
        firebase.database().ref().child(accountKey).once('value', snap => {
          console.log('accountKey account', snap.val());
          let account = snap.val();
          console.log('accountKey account.rooms', account.rooms);
          console.log('tel', tel);
          let roomkey = _.get(account, 'rooms.' + tel);
          console.log('roomkey', roomkey);
          if (roomkey) {
            firebase.database().ref().child('rooms/' + roomkey).once('value', snapshot => {
              console.log('room snapshot', snapshot.val());
              let room = snapshot.val();
              room.key = roomkey;
              deferred.resolve(room);
            });
          } else {
            UsersSvc.getMembers(tel).then((users) => {
              console.log('users', users);
              let user = users[0];
              let user_profile = users[1];
              let roomkey = firebase.database().ref().child('rooms').push().key;
              let messageUpdated = {};
              let members = [{name: user.name, surname:  user.surname},
                {name: user_profile.name, surname:  user_profile.surname}];
              let room  = {timestamp: new Date().getTime(), messages: [], members: members};
              console.log('accountKey + "/rooms/" + tel', accountKey + "/rooms/" + tel);
              console.log('/accounts/ + user.uid + "/rooms/" + tel', '/accounts/' + user.uid + "/rooms/" + tel);
              console.log("'rooms/' + roomkey", '/rooms/' + roomkey);
              console.log('room', room);
              messageUpdated[accountKey + "/rooms/" + tel] = roomkey;
              messageUpdated['/accounts/' + user.uid + "/rooms/" + user_profile.tel] = roomkey;
              messageUpdated['/rooms/' + roomkey] = {timestamp: new Date().getTime(), messages: [], members: members};
              firebase.database().ref().update(messageUpdated).then(() => {
                room.key = roomkey;
                deferred.resolve(room);
              }, () => {
                deferred.reject();
              });
            }, () => {
              deferred.reject();
            });
          }
        }, () => {
          deferred.reject();
        });
      return deferred.promise;
    },
    addMessage : (key, message) => {
      let deferred = $q.defer();
      let messageUpdated = {};
      let val = {
        date : new Date().getTime(),
        sender: FirebaseSvc.uid(),
        content: message
      };
      console.log('addMessage val', val);
      console.log('addMessage chemin', 'rooms/' + key + '/messages');

      firebase.database().ref().child('rooms/' + key + '/messages').once('value', snapshot => {
          console.log('snapshot numChildren', snapshot.numChildren());
          messageUpdated['rooms/' + key + "/messages/" + snapshot.numChildren()] = val;
          firebase.database().ref().update(messageUpdated).then(()=> {
            deferred.resolve(val);
          });
      });
      return deferred.promise;
    },
    checkUser: () => {
      let deferred = $q.defer();
      console.log('firebase.auth().currentUser', firebase.auth().currentUser)
      UsersSvc.get_current_user().then((user) => {
        console.log('checkUser', user);
        if(firebase.database().ref('accounts/' + user.email.replace('.','')).hasOwnProperty()) {
          console.log('checkUser', 'accounts/' + user.email.replace('.',''));
          firebase.database().ref('accounts/' + user.email.replace('.','')).once("value",snapshot => {
            deferred.resolve(snapshot.val());
          });
        } else {
          let pr = {
            name: user.name,
            surname: user.surname,
            email: user.email,
            rooms:{}
          };
          console.log('pr', pr);
          firebase.database().ref('accounts').child(user.email.replace('.','')).set(pr, (error) => {
            if (error) {
              // The write failed...
              console.log('error', error);
              deferred.reject();
            } else {
              // Data saved successfully!
              console.log('successfully');
              deferred.resolve(pr);
            }
          });
        }

      });
      return deferred.promise;
    }

  };
};
