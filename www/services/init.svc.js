var InitSvc = function() {
  return {
    topics : function() {
      return ['Location','Sous-location','Colocation'];
    },
    add : function() {
      return "Vous cherchez un logement parmi vos amis d'amis ?";
    },
    add2 : function() {
      return "Vous cherchez <br/> un locataire, un colocataire, un sous-locataire de confiance ?";
    },
    add3 : function() {
      return "Trouvez dans votre réseau élargi (amis, amis d'amis ...) " +
             "les personnes qui correspondent " +
	     "à vos critères de recherche.";
    },
    welcome : function() {
      return "Bienvenue sur HouseMatch";
    },
    anteCondition : function() {
      return "Touchez ‹‹ accepter & continuer ›› pour accepter ";
    },
    postCondition : function() {
      return " de HouseMatch";
    }

  };
};
