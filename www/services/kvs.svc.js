/**
 * Stockage des information de l'utilisateur sous la forme clé/valeur
 * @author Renaud Tripathi <ganeshrd@laposte.net>
 */
var KvsSvc = function($log,$q,$localStorage) {

  let store_value = (k,v) => {
                               let def = $q.defer();
                               try
                               {
                                 $localStorage[k] = v;
                                 def.resolve(true);
                               }
                               catch(e)
                               {
                                 $log.error('kvs.svc.js store failure',e);
                                 def.reject(e);
                               }
                               return def.promise;
                             };
   let get_store_value = (key) => {
                                    let deferred = $q.defer(), v = $localStorage[key];
                                    if(v != undefined) deferred.resolve(v);
	                                  else deferred.reject('undefined');
                                    return deferred.promise;
                                  };
  return {
    /**
     * Retourne la valeur $localStorage désigné par la clé en argument.
     * @param key String Une clé de l'espace $localStorage.
     * @return promise
     */
    get_store_value : get_store_value,

    /**
     * get_store_value wrapper
     */
    get_id_pwd : () => {
      return $q.all({'id':get_store_value('id'),'pwd':get_store_value('pwd')});
      },

    /**
     * Insertion d'une paire clé/valeur
     * @param k String Une clé
     * @param v any Une valeur valide pour l'espace de stockage $localStorage
     * 'clé' doit être unique dans l'espace de nom de $localStorage
     * @return promise
     */
    store_value : store_value,
    /**
     * Insertion des clés/valeurs d'un objet
     * @param o Object
     */
    store_object : (o) => {
                            let promises = Object.keys(o).map(k => { store_value(k,o[k]); });
                            return $q.all(promises);
    },
    /**
     * Supprime les entrées '_id' et 'pwd' de l'espace de stockage
     * TEST ONLY
     */
    reset_user : () => {
                         let def = $q.defer();
                         try
                         {
                           delete $localStorage._id;
                           delete $localStorage.pwd;
                           def.resolve(true);
                         }
                         catch(e)
                         {
                           $log.error('kvs.svc.js reset failure',e);
                           def.reject(e);
                         }
                         return def.promise;
                       }
  };
};
