# Librairie client du framework N2O

Cette librairie fait partie des dépendences du projet HM\_MOBILE de HOUSEMATCH.
IL s'agit d'une simple copie des scripts présent dans la distribution de N2O, le framework web
de synrc.

Elle permet d'établir et d'assurer la connection par websocket entre client et serveur (bullet.js).
Elle implémente le protocole de sérialisation BERT côté client (protocols/bert.js).
