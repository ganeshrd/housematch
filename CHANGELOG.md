### v1.1.1

 * registration redefinition (no $stateParams anymore)
 * add exit state (user, network, system)


### v1.1.0

 * add translation
 * enabled CSP in index.html and add googleapis


### v1.0.12

* user language detection XNSvc service


### v1.0.11

* fix 404 bug in tutorial when 'step' > 3
* user info in zombie mode view
* reset user in zombie mode


### v1.0.9

* CHANGELOG  creation.


### v1.0.8

* $localStorage implementation.
* KvsSvc uses $localStorage from now ( any service using DbSvc is now obsolete ).
* 'zombie mode' is accessible from user menu.
* 'www/lib' added to the gitignore. Thus hm and n2o libraries need to 'bowerified'.
