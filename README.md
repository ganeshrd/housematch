# housematch

The final front end repository for Housematch

```
> git clone git@bitbucket.org:ganeshrd/housematch.git
> cd housematch
> npm install gulp --save-dev
> gulp
> bower install
> cp -R hm www/lib/
> cp -R n2o www/lib/
> ionic serve
```

You may set the proper server address in `www/index.html` before build or serve.
